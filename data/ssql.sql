/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3306
 Source Schema         : librarymanager

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 30/08/2020 14:25:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ssql
-- ----------------------------
DROP TABLE IF EXISTS `ssql`;
CREATE TABLE `ssql`  (
  `id` int(11) NOT NULL,
  `status` int(255) NULL DEFAULT 0 COMMENT '1为开启自动备份，0为关闭',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ssql
-- ----------------------------
INSERT INTO `ssql` VALUES (1, 0);

SET FOREIGN_KEY_CHECKS = 1;
