-- MySQL dump 10.10
--
-- Host: localhost    Database: librarymanager
-- ------------------------------------------------------
-- Server version	5.0.22-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `librarymanager`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `librarymanager` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `librarymanager`;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL auto_increment COMMENT '编号',
  `userName` varchar(255) default NULL COMMENT '用户名',
  `password` varchar(255) default NULL COMMENT '登录密码',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--


/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
LOCK TABLES `admin` WRITE;
INSERT INTO `admin` VALUES (2,'lisiliu','147258369'),(3,'liuwu','159753852');
UNLOCK TABLES;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `bookNumber` varchar(50) default NULL COMMENT '图书编号',
  `bookName` varchar(255) default NULL COMMENT '图书名称',
  `pinYinCode` varchar(255) default NULL COMMENT '拼音简码',
  `bookCount` int(50) default NULL COMMENT '图书现存数量',
  `status` int(50) default NULL COMMENT '图书可借状态，0代表不可借，1代表可借',
  `author` varchar(255) default NULL COMMENT '图书作者',
  `bookType` int(50) default NULL COMMENT '图书类型',
  `maxCount` int(50) default NULL COMMENT '最大数量',
  `bookIcon` varchar(255) default NULL COMMENT '图书图片',
  `description` varchar(255) default NULL COMMENT '备注',
  `location` varchar(255) default NULL COMMENT '图书所在位置',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--


/*!40000 ALTER TABLE `books` DISABLE KEYS */;
LOCK TABLES `books` WRITE;
INSERT INTO `books` VALUES (1,'1001','西游记','XYJ',31,1,'吴承恩',3,10,NULL,'阿萨达','一楼一区'),(2,'1002','大话西游','DHXY',30,1,'老爱',4,10,NULL,'实打实收电费','一楼二区');
UNLOCK TABLES;
/*!40000 ALTER TABLE `books` ENABLE KEYS */;

--
-- Table structure for table `borrow`
--

DROP TABLE IF EXISTS `borrow`;
CREATE TABLE `borrow` (
  `id` int(20) NOT NULL auto_increment COMMENT '序号',
  `bookId` varchar(50) default NULL COMMENT '图书编号',
  `personId` int(11) default NULL COMMENT '读者编号',
  `bookName` varchar(255) character set utf8 collate utf8_unicode_ci default NULL COMMENT '图书名称',
  `personName` varchar(255) character set utf8 collate utf8_unicode_ci default NULL COMMENT '读者名称',
  `bookType` varchar(255) character set utf8 collate utf8_unicode_ci default NULL COMMENT '图书类别',
  `borrowTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '借书时间',
  `shouldReturnTime` timestamp NOT NULL default '0000-00-00 00:00:00' COMMENT '应还时间',
  `status` int(2) default NULL COMMENT '是否归还(0:未还 1:一还)',
  `actualReturnTime` timestamp NOT NULL default '0000-00-00 00:00:00' COMMENT '归还时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `borrow`
--


/*!40000 ALTER TABLE `borrow` DISABLE KEYS */;
LOCK TABLES `borrow` WRITE;
INSERT INTO `borrow` VALUES (1,'1001',1,'西游记','李四','都市','2020-09-11 00:47:31','2020-10-11 00:45:43',1,'2020-09-11 00:47:31'),(2,'1002',4,'大话西游','马秋阳','言情','2020-09-11 00:57:22','2020-10-11 00:53:14',0,'2020-09-11 00:57:17'),(3,'1001',5,'西游记','无语','都市','2020-09-11 01:04:17','2020-10-01 01:03:59',1,'2020-09-11 01:03:59'),(4,'1001',6,'西游记','李四五','都市','2020-09-11 01:06:19','2020-10-11 01:05:58',1,'2020-09-11 01:05:58'),(5,'1002',6,'大话西游','李四五','言情','2020-09-11 01:07:15','2020-10-11 01:06:01',1,'2020-09-11 01:06:01'),(6,'1001',6,'西游记','李四五','都市','2020-09-11 01:12:57','2020-10-11 01:06:04',1,'2020-09-11 01:06:04');
UNLOCK TABLES;
/*!40000 ALTER TABLE `borrow` ENABLE KEYS */;

--
-- Table structure for table `dictionary`
--

DROP TABLE IF EXISTS `dictionary`;
CREATE TABLE `dictionary` (
  `id` int(11) NOT NULL auto_increment COMMENT '编号',
  `name` varchar(255) default NULL COMMENT '名称',
  `typeid` int(11) default NULL COMMENT '类型编号',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dictionary`
--


/*!40000 ALTER TABLE `dictionary` DISABLE KEYS */;
LOCK TABLES `dictionary` WRITE;
INSERT INTO `dictionary` VALUES (3,'都市',0),(4,'言情',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `dictionary` ENABLE KEYS */;

--
-- Table structure for table `orderlist`
--

DROP TABLE IF EXISTS `orderlist`;
CREATE TABLE `orderlist` (
  `id` int(20) NOT NULL auto_increment COMMENT '序号',
  `orderNo` varchar(20) character set utf8 collate utf8_unicode_ci default NULL COMMENT '单号',
  `bookId` varchar(20) character set utf8 collate utf8_unicode_ci default NULL COMMENT '图书编号',
  `bookName` varchar(255) character set utf8 collate utf8_unicode_ci default NULL COMMENT '图书名称',
  `bookType` varchar(255) character set utf8 collate utf8_unicode_ci default NULL COMMENT '图书类别',
  `number` int(5) default NULL COMMENT '入库数量',
  `purchasePrice` double(10,2) default NULL COMMENT '进价',
  `totalPrice` double(10,2) default NULL COMMENT '总金额',
  `supplier` varchar(255) character set utf8 collate utf8_unicode_ci default NULL COMMENT '供货商',
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '入库日期',
  `note` varchar(255) character set utf8 collate utf8_unicode_ci default NULL COMMENT '备注',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderlist`
--


/*!40000 ALTER TABLE `orderlist` DISABLE KEYS */;
LOCK TABLES `orderlist` WRITE;
INSERT INTO `orderlist` VALUES (1,'202009110929005661','1001','西游记','都市',20,2.00,40.00,'1','2020-09-11 01:31:03','这本书老便宜了'),(2,'202009110937043953','1002','大话西游','言情',20,2.00,40.00,'1','2020-09-11 01:37:04','咋项目部撒娇的保时捷');
UNLOCK TABLES;
/*!40000 ALTER TABLE `orderlist` ENABLE KEYS */;

--
-- Table structure for table `reader`
--

DROP TABLE IF EXISTS `reader`;
CREATE TABLE `reader` (
  `id` int(11) NOT NULL auto_increment COMMENT '编号',
  `name` varchar(255) default NULL COMMENT '姓名',
  `gender` varchar(255) default NULL COMMENT '性别',
  `username` varchar(255) default NULL COMMENT '用户名',
  `password` varchar(255) default NULL COMMENT '登录密码',
  `borrowtime` int(11) default NULL COMMENT '借书期限',
  `borrowstart` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP COMMENT '开始借书时间',
  `type` varchar(255) default NULL COMMENT '类别(学生,老师)',
  `num` int(11) default NULL COMMENT '类别下限制的借书数量(学生:3;老师:6)',
  `nreturnnum` int(11) default NULL COMMENT '未还数量',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reader`
--


/*!40000 ALTER TABLE `reader` DISABLE KEYS */;
LOCK TABLES `reader` WRITE;
INSERT INTO `reader` VALUES (1,'李四','男','lisi','1234567',30,'2020-08-30 04:53:22','老师',5,0),(3,'李三','女','lisan','123456',30,'2020-08-30 09:17:08','老师',5,0),(4,'马秋阳','男','mqy','159853',30,'2020-08-30 09:39:10','老师',5,2),(5,'无语','男','wuyu','15965',20,'2020-08-30 09:28:00','学生',3,0),(6,'李四五','男','lisiwu','123456',30,'2020-09-04 08:09:52','老师',5,-2),(7,'马克','女','make','852963.0',20,'2020-08-30 09:30:56','学生',3,0),(8,'杨幂','女','yangmi','1630610410',20,'2020-08-30 09:32:24','学生',3,0),(10,'流向','男','liux','159385285',20,'2020-09-04 08:10:21','学生',3,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `reader` ENABLE KEYS */;

--
-- Table structure for table `ssql`
--

DROP TABLE IF EXISTS `ssql`;
CREATE TABLE `ssql` (
  `id` int(11) NOT NULL,
  `status` int(255) default '0' COMMENT '1为开启自动备份，0为关闭',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ssql`
--


/*!40000 ALTER TABLE `ssql` DISABLE KEYS */;
LOCK TABLES `ssql` WRITE;
INSERT INTO `ssql` VALUES (1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `ssql` ENABLE KEYS */;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `abbre` varchar(255) default NULL COMMENT '简称',
  `name` varchar(255) default NULL COMMENT '名称',
  `address` varchar(255) default NULL COMMENT '地址',
  `contacts` varchar(255) default NULL COMMENT '联系人',
  `Tnumber` varchar(20) default NULL COMMENT '电话号码',
  `Cnumber` varchar(20) default NULL COMMENT '手机号',
  `ACnumber` varchar(20) default NULL COMMENT '银行卡号',
  `remarks` varchar(255) default NULL COMMENT '备注',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier`
--


/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
LOCK TABLES `supplier` WRITE;
INSERT INTO `supplier` VALUES (1,'河科院','河南科技学院','河南省新乡市','李白','159852745','15985236587','15648586585314654','打法是否');
UNLOCK TABLES;
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

