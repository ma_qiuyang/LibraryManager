/*
 Navicat Premium Data Transfer

 Source Server         : MySQL_01
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3309
 Source Schema         : librarymanager

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 24/08/2020 18:51:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for borrow
-- ----------------------------
DROP TABLE IF EXISTS `borrow`;
CREATE TABLE `borrow`  (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `bookId` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图书编号',
  `personId` int(11) DEFAULT NULL COMMENT '读者编号',
  `bookName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图书名称',
  `personName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '读者名称',
  `bookType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图书类别',
  `borrowTime` timestamp(0) DEFAULT NULL COMMENT '借书时间',
  `shouldReturnTime` timestamp(0) DEFAULT NULL COMMENT '应还时间',
  `status` int(2) DEFAULT NULL COMMENT '是否归还(0:未还 1:一还)',
  `actualReturnTime` timestamp(0) DEFAULT NULL COMMENT '归还时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
