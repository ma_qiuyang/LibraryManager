CREATE TABLE `dictionary` (
  `id` int(11) NOT NULL COMMENT '编号',
  `name` varchar(255) default NULL COMMENT '名称',
  `typeid` int(11) default NULL COMMENT '类型编号',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;