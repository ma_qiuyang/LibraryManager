CREATE TABLE `reader` (
  `id` int(11) NOT NULL COMMENT '编号',
  `name` varchar(255) default NULL COMMENT '姓名',
  `gender` varchar(255) default NULL COMMENT '性别',
  `username` varchar(255) default NULL COMMENT '用户名',
  `password` varchar(255) default NULL COMMENT '登录密码',
  `borrowtime` int(11) default NULL COMMENT '借书期限',
  `borrowstart` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '开始借书时间',
  `type` varchar(255) default NULL COMMENT '类别(学生,老师)',
  `num` int(11) default NULL COMMENT '类别下限制的借书数量(学生:3;老师:6)',
  `nreturnnum` int(11) default NULL COMMENT '未还数量',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;