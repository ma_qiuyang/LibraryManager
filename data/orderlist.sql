CREATE TABLE `orderlist` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `orderNo` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '单号',
  `bookId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图书编号',
  `bookName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图书名称',
  `bookType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图书类别',
  `number` int(5) DEFAULT NULL COMMENT '入库数量',
  `purchasePrice` double(10,2) DEFAULT NULL COMMENT '进价',
  `totalPrice` double(10,2) DEFAULT NULL COMMENT '总金额',
  `supplier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '供货商',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '入库日期',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;