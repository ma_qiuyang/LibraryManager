package org.lq.entity;

import java.sql.Timestamp;
/**
 * 读者表
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月19日下午6:47:34
 * @package org.lq.entity
 */
public class Reader {
	
	private int id;//编号
	private String name;//姓名
	private String gender;//性别
	private String userName;//用户名
	private String passWord;//登录密码
	private int borrowTime;//借书期限
	private Timestamp borrowStart;//开始借书时间
	private String type;//类别(学生,老师)
	private int num;//类别下限制的借书数量(学生:3;老师:6)
	private int notreturnnum;//未还数量
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public int getBorrowTime() {
		return borrowTime;
	}
	public void setBorrowTime(int borrowTime) {
		this.borrowTime = borrowTime;
	}
	public Timestamp getBorrowStart() {
		return borrowStart;
	}
	public void setBorrowStart(Timestamp borrowStart) {
		this.borrowStart = borrowStart;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getNotreturnnum() {
		return notreturnnum;
	}
	public void setNotreturnnum(int notreturnnum) {
		this.notreturnnum = notreturnnum;
	}
	
}
