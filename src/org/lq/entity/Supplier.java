package org.lq.entity;
/**
 * 供应商基础表
 * @author 付金晨
 */
public class Supplier {
	private int id;//序号
	private String abbre;//简称
	private String name;//名称
	private String address;//地址
	private String contacts;//联系人
	private String Tnumber;//电话号
	private String Cnumber;//手机号
	private String ACnumber;//银行卡号
	private String remarks;//备注
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAbbre() {
		return abbre;
	}
	public void setAbbre(String abbre) {
		this.abbre = abbre;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContacts() {
		return contacts;
	}
	public void setContacts(String contacts) {
		this.contacts = contacts;
	}
	public String getTnumber() {
		return Tnumber;
	}
	public void setTnumber(String tnumber) {
		Tnumber = tnumber;
	}
	public String getCnumber() {
		return Cnumber;
	}
	public void setCnumber(String cnumber) {
		Cnumber = cnumber;
	}
	public String getACnumber() {
		return ACnumber;
	}
	public void setACnumber(String aCnumber) {
		ACnumber = aCnumber;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@Override
	public String toString() {
		return name;
	}
}
