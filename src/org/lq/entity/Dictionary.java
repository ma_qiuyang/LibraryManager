package org.lq.entity;

/**
 * 字典表
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月19日下午6:47:50
 * @package org.lq.entity
 */
public class Dictionary {
	
	private int id;//编号
	private String name;//名称
	private int typeid;//类型编号
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTypeid() {
		return typeid;
	}
	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}
	public void setTypeid(TypeID type) {
		this.typeid = type.getId();
	}
	@Override
	public String toString() {
		return name;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj==null) {
			return false;
		}else if(!(obj instanceof Dictionary)) {
			return false;
		}
		
		Dictionary d = (Dictionary) obj;
		
		return this.name.equals(d.getName());
	}
	
}
