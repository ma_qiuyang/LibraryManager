package org.lq.entity;

/**
 * 图书实体类
 * 
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.entity
 * @date 2020年8月18日下午12:12:59
 */
public class Books {

	private int id;// 序号
	private String bookNumber;// 图书编号
	private String bookName;// 图书名称
	private String pinYinCode;// 拼音简码
	private int bookCount;// 图书现存数量
	private int status;// 图书可借状态，0代表不可借，1代表可借
	private String author;// 图书作者
	private int bookType;// 图书类型
	private int maxCount;// 最大数量
	private String bookIcon;// 图书图片
	private String description;// 备注
	private String location;//图书所在位置
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBookNumber() {
		return bookNumber;
	}
	public void setBookNumber(String bookNumber) {
		this.bookNumber = bookNumber;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public int getBookCount() {
		return bookCount;
	}
	public void setBookCount(int bookCount) {
		this.bookCount = bookCount;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getBookType() {
		return bookType;
	}
	public void setBookType(int bookType) {
		this.bookType = bookType;
	}
	public int getMaxCount() {
		return maxCount;
	}
	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}
	public String getBookIcon() {
		return bookIcon;
	}
	public void setBookIcon(String bookIcon) {
		this.bookIcon = bookIcon;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPinYinCode() {
		return pinYinCode;
	}
	public void setPinYinCode(String pinYinCode) {
		this.pinYinCode = pinYinCode;
	}
	@Override
	public String toString() {
		return "Books [id=" + id + ", bookNumber=" + bookNumber + ", bookName=" + bookName + ", pinYinCode="
				+ pinYinCode + ", bookCount=" + bookCount + ", status=" + status + ", author=" + author + ", bookType="
				+ bookType + ", maxCount=" + maxCount + ", bookIcon=" + bookIcon + ", description=" + description
				+ ", location=" + location + "]";
	}

	
	

}
