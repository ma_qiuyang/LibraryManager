package org.lq.entity;

import java.sql.Timestamp;
/**
 * 借阅图书实体类
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.entity
 * @时间 2020年8月18日
 *
 */
public class Borrow {
	private int id;  //序号
	private String bookId; //图书编号
	private int personId; //读者编号
	private String bookName; //图书名称
	private String personName; //读者姓名
	private String bookType; //图书类型
	private Timestamp borrowTime; //借阅日期
	private Timestamp shouldReturnTime; //应还日期
	private int status; //是否归还(0:未还 1:已归还)
	private Timestamp actualReturnTime; //实际归还日期
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getBookType() {
		return bookType;
	}
	public void setBookType(String bookType) {
		this.bookType = bookType;
	}
	public Timestamp getBorrowTime() {
		return borrowTime;
	}
	public void setBorrowTime(Timestamp borrowTime) {
		this.borrowTime = borrowTime;
	}
	public Timestamp getShouldReturnTime() {
		return shouldReturnTime;
	}
	public void setShouldReturnTime(Timestamp shouldReturnTime) {
		this.shouldReturnTime = shouldReturnTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Timestamp getActualReturnTime() {
		return actualReturnTime;
	}
	public void setActualReturnTime(Timestamp actualReturnTime) {
		this.actualReturnTime = actualReturnTime;
	}
	
}
