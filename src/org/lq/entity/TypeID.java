package org.lq.entity;
/**
 *  字典类型枚举
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月23日上午11:11:43
 * @package org.lq.entity
 */
public enum TypeID {
	
	图书类别(0),默认(-1);
	
	private int id;
	private TypeID(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	
}
