package org.lq.service;

import java.util.Date;
import java.util.List;

import org.lq.entity.OrderList;
/**
 * 图书图库数据访问层接口
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.service
 * @时间 2020年8月18日
 *
 */
public interface OrderListService {
	/**
	 * 添加
	 * @param t
	 * @return
	 */
	boolean save(OrderList orderList);

	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean update(OrderList orderList);

	/**
	 * 删除
	 * @param t
	 * @return
	 */
	boolean delete(int id);

	/**
	 * 查询全部
	 * @return
	 */
	List<OrderList> findAll();

	/**
	 * 通过id查询
	 * @param id
	 * @return
	 */
	OrderList getById(int id);
	
	/**
	 * 通过时间查询
	 * @param start
	 * @param end
	 * @return
	 */
	List<OrderList> getOrderByDate(Date start,Date end);
	
	/**
	 * 根据时间查询
	 * @param type 1: 当天 2: 当月 3: 当年
	 * @return
	 */
	List<OrderList> getByDateName(String type);
}
