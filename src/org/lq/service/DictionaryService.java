package org.lq.service;

import java.util.List;

import org.lq.entity.Dictionary;
import org.lq.entity.TypeID;

public interface DictionaryService {
	
	/**
	 * 添加
	 * @param t
	 * @return
	 */
	boolean save(Dictionary t);

	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean update(Dictionary t);

	/**
	 * 删除
	 * @param t
	 * @return
	 */
	boolean delete(int id);

	/**
	 * 查询全部
	 * @return
	 */
	List<Dictionary> findAll();

	/**
	 * 通过id查询
	 * @param id
	 * @return
	 */
	Dictionary getById(int id);
	/**
	 * 根据typeid查询
	 * @param tid
	 * @return
	 */
	List<Dictionary> findByTypeid(TypeID t);
	/**
	 * 根据名称和typeid查询
	 * @param name
	 * @param tid
	 * @return
	 */
	Dictionary getByTypeidAndName(String name,TypeID t);
	/**
	 * 根据名称查询
	 * @param name
	 * @return
	 */
	boolean getByName(String name);
}
