package org.lq.service;

import java.util.List;

import org.lq.entity.Admin;


/**
 *  管理员业务层接口
 * @author 付金晨
 *
 */

public interface AdminServiceDao {
	
	/**
	 * 添加
	 * 
	 * @param admin
	 * @return
	 */
	boolean save(Admin admin);

	/**
	 * 修改
	 * 
	 * @param admin
	 * @return
	 */
	boolean update(Admin admin);

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	boolean delete(int id);

	/**
	 * 查询所有
	 * 
	 * @return
	 */
	List<Admin> findAll();

	/**
	 * 查询指定id管理员
	 * 
	 * @param id
	 * @return
	 */
	Admin getById(int id);
	/**
	 * 根据用户名和密码登录
	 * @param username
	 * @param password
	 * @return
	 */
	Admin getByUser(String username,String password);

}
