package org.lq.service;

import java.util.List;

import org.lq.entity.Supplier;

/**
 * 供应商业务接口
 * @author 付金晨
 */
public interface SupplierService {
	
	/**
	 * 	添加
	 * @param t
	 * @return
	 */
	boolean saveSupplier(Supplier t);
	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean updateSupplier(Supplier t);
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	boolean deleteSupplierById(int id);
	/**
	 * 查询全部
	 * @return
	 */
	List<Supplier> findSupplierAll();
	/**
	 * 通过ID查询
	 * @param id
	 * @return
	 */
	Supplier getSupplierById(int id);
	
	/**
	 *	通过名称模糊查询
	 * @param name
	 * @return
	 */
	List<Supplier> likeSupplierByName(String name);
}
