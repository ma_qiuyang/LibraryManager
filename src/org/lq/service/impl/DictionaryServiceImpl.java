package org.lq.service.impl;

import java.util.List;

import org.lq.dao.DictionaryDao;
import org.lq.dao.impl.DictionaryDaoImpl;
import org.lq.entity.Dictionary;
import org.lq.entity.TypeID;
import org.lq.service.DictionaryService;

public class DictionaryServiceImpl implements DictionaryService {
	
	private DictionaryDao ddao = new DictionaryDaoImpl();
	
	@Override
	/**
	 * 添加
	 */
	public boolean save(Dictionary t) {
		return ddao.save(t)>0;
	}

	@Override
	/**
	 * 修改
	 */
	public boolean update(Dictionary t) {
		return ddao.update(t)>0;
	}

	@Override
	/**
	 * 删除
	 */
	public boolean delete(int id) {
		return ddao.delete(id)>0;
	}

	@Override
	/**
	 * 查询所有
	 */
	public List<Dictionary> findAll() {
		return ddao.findAll();
	}

	@Override
	/**
	 * 根据id查询
	 */
	public Dictionary getById(int id) {
		return ddao.getById(id);
	}

	@Override
	/**
	 * 根据typeid查询
	 */
	public List<Dictionary> findByTypeid(TypeID t) {
		return ddao.findByTypeid(t.getId());
	}

	@Override
	/**
	 * 根据名称和typeid查询
	 */
	public Dictionary getByTypeidAndName(String name, TypeID t) {
		return ddao.getByTypeidAndName(name, t.getId());
	}

	@Override
	public boolean getByName(String name) {
		boolean bool = true;
		try {
			//如果取不到值,则返回false ,反之,返回true
			ddao.getByName(name);
			bool = true;
		} catch (Exception e) {
			bool = false;
		}
		return bool;
	}

}
