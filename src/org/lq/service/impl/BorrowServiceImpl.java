package org.lq.service.impl;

import java.util.List;

import org.lq.dao.BorrowDao;
import org.lq.dao.impl.BorrowDaoImpl;
import org.lq.entity.Books;
import org.lq.entity.Borrow;
import org.lq.service.BooksService;
import org.lq.service.BorrowService;
/**
 * 图书借阅业务层接口实现
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.service.impl
 * @时间 2020年8月18日
 *
 */
public class BorrowServiceImpl implements BorrowService {

	private BorrowDao bdao = new BorrowDaoImpl();
	private BooksService bs = new BooksServiceImpl();
	
	@Override
	public boolean save(Borrow borrow) {
		return bdao.save(borrow) > 0;
	}

	@Override
	public boolean update(Borrow borrow) {
		return bdao.update(borrow) > 0;
	}

	@Override
	public boolean delete(int id) {
		return bdao.delete(id) > 0;
	}

	@Override
	public List<Borrow> findAll() {
		return bdao.findAll();
	}

	@Override
	public Borrow getById(int id) {
		return bdao.getById(id);
	}

	@Override
	public List<Borrow> getByPersonId(int personId) {
		return bdao.getByPersonId(personId);
	}

	@Override
	public List<Borrow> getByStatus(int personId, int status) {
		return bdao.getByStatus(personId, status);
	}

}
