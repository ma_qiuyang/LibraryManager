package org.lq.service.impl;

import java.util.List;

import org.lq.dao.SupplierDao;
import org.lq.dao.impl.SupplierDaoImpl;
import org.lq.entity.Supplier;
import org.lq.service.SupplierService;
/**
 * 供应商业务实现类
 * @author 付金晨
 */
public class SupplierServiceImpl implements SupplierService {
	private SupplierDao sd = new SupplierDaoImpl();
	/**
	 * 添加供应商
	 */
	@Override
	public boolean saveSupplier(Supplier s) {
		return sd.save(s)>0;
	}
	
	/**
	 * 修改供应商
	 */

	@Override
	public boolean updateSupplier(Supplier s) {
		return sd.update(s)>0;
	}

	/**
	 * 删除供应商
	 */
	@Override
	public boolean deleteSupplierById(int id) {
		return sd.delete(id)>0;
	}

	/**
	 * 查询全部供应商
	 */
	@Override
	public List<Supplier> findSupplierAll() {
		return sd.findAll();
	}

	@Override
	public Supplier getSupplierById(int id) {
		return sd.getById(id);
	}

	@Override
	public List<Supplier> likeSupplierByName(String name) {
		return sd.likeSupplierByName(name);
	}

}
