package org.lq.service.impl;

import java.util.List;

import org.lq.dao.ReaderDao;
import org.lq.dao.impl.ReaderDaoImpl;
import org.lq.entity.Reader;
import org.lq.service.ReaderService;
/**
 * 读者业务层实现类
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月23日上午11:01:11
 * @package org.lq.service.impl
 */
public class ReaderServiceImpl implements ReaderService {
	
	private ReaderDao rdao = new ReaderDaoImpl();
	
	@Override
	/**
	 * 添加
	 */
	public boolean save(Reader rd) {
		return rdao.save(rd)>0;
	}

	@Override
	/**
	 * 修改
	 */
	public boolean update(Reader rd) {
		return rdao.update(rd)>0;
	}

	@Override
	/**
	 * 删除
	 */
	public boolean delete(int id) {
		return rdao.delete(id)>0;
	}

	@Override
	/**
	 * 查询所有
	 */
	public List<Reader> findAll() {
		return rdao.findAll();
	}

	@Override
	/**
	 * 根据id查询
	 */
	public Reader getById(int id) {
		return rdao.getById(id);
	}

	@Override
	/**
	 * 根据用户名和密码登录
	 */
	public Reader login(String userName, String passWord) {
		return rdao.login(userName, passWord);
	}

	@Override
	/**
	 * 根据名称查询
	 */
	public List<Reader> getByName(String name) {
		return rdao.getByName(name);
	}

	@Override
	/**
	 * 根据类别查询
	 */
	public List<Reader> getByType(String type) {
		return rdao.getByType(type);
	}

}
