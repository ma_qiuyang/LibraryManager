package org.lq.service.impl;

import java.util.List;

import org.lq.dao.AdminDao;
import org.lq.dao.impl.AdminDaoImpl;
import org.lq.entity.Admin;
import org.lq.service.AdminServiceDao;

/**
 *  管理员业务层接口实现
 * @author 付金晨
 *
 */

public class AdminServiceDaoImpl implements AdminServiceDao {
	
	AdminDao ad = new AdminDaoImpl();

	@Override
	public boolean save(Admin admin) {
		return ad.save(admin) > 0;
	}

	@Override
	public boolean update(Admin admin) {
		return ad.update(admin) > 0;
	}

	@Override
	public boolean delete(int id) {
		return ad.delete(id) > 0;
	}

	@Override
	public List<Admin> findAll() {
		return ad.findAll();
	}

	@Override
	public Admin getById(int id) {
		return ad.getById(id);
	}

	@Override
	public Admin getByUser(String username, String password) {
		return ad.getByUser(username, password);
	}

}
