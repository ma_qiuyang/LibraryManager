package org.lq.service.impl;

import org.lq.dao.SqlDao;
import org.lq.dao.impl.SqlDaoImpl;
import org.lq.entity.Sql;
import org.lq.service.SqlService;

public class SqlServiceImpl implements SqlService {
	SqlDao sd = new SqlDaoImpl();
	@Override
	public boolean update(Sql s) {
		return sd.update(s)>0;
	}

	@Override
	public Sql getById(int id) {
		return sd.getById(id);
	}

}
