package org.lq.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.lq.dao.BooksDao;
import org.lq.dao.impl.BooksDaoImpl;
import org.lq.entity.Books;
import org.lq.service.BooksService;

/**
 * 图书类业务层接口实现
 * 
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.service.Impl
 * @date 2020年8月18日下午1:48:11
 */
public class BooksServiceImpl implements BooksService {
	BooksDao bd = new BooksDaoImpl();

	@Override
	public boolean save(Books book) {
		return bd.save(book) > 0;
	}

	@Override
	public boolean update(Books book) {
		return bd.update(book) > 0;
	}

	@Override
	public boolean delete(int id) {
		return bd.delete(id) > 0;
	}

	@Override
	public List<Books> findAll() {
		return bd.findAll();
	}

	@Override
	public Books getById(int id) {
		return bd.getById(id);
	}
	
	@Override
	public List<Books> getByBookNumber(String bookNumber) {
		return bd.getByBookNumber(bookNumber);
	}

	@Override
	public List<Books> getByBookName(String name) {
		return bd.getByBookName(name);
	}

	@Override
	public List<Books> getByPinYinCode(String value) {
		return bd.getByPinYinCode(value);
	}


	@Override
	public List<Books> getByBookType(int value) {
		return bd.getByBookType(value);
	}

	@Override
	public List<Books> getByStatus(int value) {
		return bd.getByStatus(value);
	}

	@Override
	public List<Books> getByAuthor(String value) {
		return bd.getByAuthor(value);
	}

	@Override
	public List<Books> getByBookNumber(String bookNumber, int status) {
		return bd.getByBookNumber(bookNumber, status);
	}

	@Override
	public List<Books> getByBookName(String name, int status) {
		return bd.getByBookName(name, status);
	}

	@Override
	public List<Books> getByAuthor(String value, int status) {
		return bd.getByAuthor(value, status);
	}

	@Override
	public List<Books> getByBookType(int value, int status) {
		return bd.getByBookType(value, status);
	}

	@Override
	public List<Books> query(String value) {
		List<Books> list = new ArrayList<Books>();
		//根据正则表达式判断使用哪种查询方式，三种方式都可以进行模糊查询
//		String codePattern = "\\d{13}";
		String codePattern = "[0-9]*";
		String pinYinPattern = "\\w+";
		String chinesePattern = ".*[\\u4e00-\\u9fa5].*";
		
		if(Pattern.matches(codePattern, value)) {
			//编码查询
			list = bd.getByBookNumber(value);
//			System.out.println(1);
		}else if(Pattern.matches(pinYinPattern, value)) {
			//拼音查询
			list = bd.getByPinYinCode(value);
//			System.out.println(2);
		}else if(Pattern.matches(chinesePattern, value)){
			//商品名称查询
			list = bd.getByBookName(value);
//			System.out.println(3);
		}else {
			list = findAll();
		}
		return list;
	}

	@Override
	public boolean getBybookType(int bookType) {
		boolean bool = true;
		try {
			//如果取不到值,则返回false ,反之,返回true
			//如果为true,则有值,不能删除
			bd.getBybookType(bookType);
			bool = true;
		} catch (Exception e) {
			bool = false;
		}
		return bool;
	}

	@Override
	public boolean getByBookNumberExactly(String bookNumber) {
		if (bd.getByBookNumberExactly(bookNumber)==null) {
			return false;
		}else {
			return true ;
		}
	}

	@Override
	public Books getBookByBookNumber(String bookNumber) {
		return bd.getByBookNumberExactly(bookNumber);
	}


}
