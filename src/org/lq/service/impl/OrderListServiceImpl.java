package org.lq.service.impl;

import java.util.Date;
import java.util.Calendar;
import java.util.List;

import org.lq.dao.OrderListDao;
import org.lq.dao.impl.OrderListDaoImpl;
import org.lq.entity.Books;
import org.lq.entity.OrderList;
import org.lq.service.BooksService;
import org.lq.service.OrderListService;
/**
 * 图书入库业务层接口实现
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.service.impl
 * @时间 2020年8月18日
 *
 */
public class OrderListServiceImpl implements OrderListService {
	
	private OrderListDao odao = new OrderListDaoImpl();
	
	@Override
	public boolean save(OrderList orderList) {
		return odao.save(orderList) > 0;
	}

	@Override
	public boolean update(OrderList orderList) {
		return odao.update(orderList) > 0;
	}

	@Override
	public boolean delete(int id) {
		return odao.delete(id) > 0;
	}

	@Override
	public List<OrderList> findAll() {
		return odao.findAll();
	}

	@Override
	public OrderList getById(int id) {
		return odao.getById(id);
	}

	@Override
	public List<OrderList> getOrderByDate(Date start, Date end) {
		return odao.getOrderByDate(start, end);
	}

	@Override
	public List<OrderList> getByDateName(String type) {
		List<OrderList> list = null;
		Date start = null;
		Date end = null;
		switch (type) {
			case "显示所有": {
				list = findAll();
				break;
			}
			case "显示当年":{
				Calendar c1 = Calendar.getInstance();
				int year = c1.get(Calendar.YEAR);
				//此构造给的参数为从1989年12月31日 0点0分0秒开始算起,加上多少
				//给year-1900,0,1 即为2020年1月1日的0点0分0秒
				start = new Date(year-1900, 0, 1);
				//此处为2021年的起始
				end = new Date(year+1 -1900, 0, 1);
				list = getOrderByDate(start, end);
				break;
			}
			case "显示当月":{
				Calendar c2 = Calendar.getInstance();
				c2.add(Calendar.MONTH, 0);
				c2.set(Calendar.DAY_OF_MONTH, 0);
				start = new Date(c2.getTimeInMillis());
				
				Calendar c3 = Calendar.getInstance();
				c3.set(Calendar.DAY_OF_MONTH, c3.getActualMaximum(Calendar.DAY_OF_MONTH));
				end = new Date(c3.getTimeInMillis());
				
				list = odao.getOrderByDate(start, end);
				break;
			}
			case "显示当日":{
				Calendar c4 = Calendar.getInstance();
				start = new Date(c4.getTimeInMillis());
				
				Calendar c5 = Calendar.getInstance();
				c5.add(Calendar.DAY_OF_MONTH, 1);
				end = new Date(c5.getTimeInMillis());
				list = getOrderByDate(start, end);
				break;
			}
		}
		return list;
	}

}
