package org.lq.service;

import java.util.List;

import org.lq.entity.Borrow;

/**
 * 图书借阅业务层接口
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.service
 * @时间 2020年8月18日
 *
 */
public interface BorrowService {
	/**
	 * 添加
	 * @param t
	 * @return
	 */
	boolean save(Borrow borrow);

	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean update(Borrow borrow);

	/**
	 * 删除
	 * @param t
	 * @return
	 */
	boolean delete(int id);

	/**
	 * 查询全部
	 * @return
	 */
	List<Borrow> findAll();

	/**
	 * 通过id查询
	 * @param id
	 * @return
	 */
	Borrow getById(int id);
	/**
	 * 根据图书状态(已还或者未还)获取借阅历史
	 * @param status
	 * @return
	 */
	List<Borrow> getByStatus(int personId, int status);
	/**
	 * 根据借阅人查询借阅历史
	 * @param personId
	 * @return
	 */
	List<Borrow> getByPersonId(int personId);
}
