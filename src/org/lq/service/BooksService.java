package org.lq.service;

import java.util.List;

import org.lq.entity.Books;

/**
 * 图书类业务层接口
 * 
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.service
 * @date 2020年8月18日下午1:45:59
 */
public interface BooksService {
	/**
	 * 添加图书
	 * 
	 * @param book
	 * @return
	 */
	boolean save(Books book);

	/**
	 * 修改图书信息
	 * 
	 * @param book
	 * @return
	 */
	boolean update(Books book);

	/**
	 * 根据id删除图书
	 * 
	 * @param id
	 * @return
	 */
	boolean delete(int id);

	/**
	 * 查询所有图书
	 * 
	 * @return
	 */
	List<Books> findAll();

	/**
	 * 查询指定id图书
	 * 
	 * @param id
	 * @return
	 */
	Books getById(int id);
	/**
	 * 根据图书类型查询
	 * @param bookType
	 * @return
	 */
	boolean getBybookType(int bookType);
	
	/**
	 * 根据图书编号进行模糊查询
	 * @param id
	 * @return
	 */
	List<Books> getByBookNumber(String bookNumber);
	/**
	 * 根据图书名称进行模糊查询
	 * @param name
	 * @return
	 */
	List<Books> getByBookName(String name);
	/**
	 * 根据拼音简码进行模糊查询
	 * @param value
	 * @return
	 */
	List<Books> getByPinYinCode(String value);
	/**
	 * 查询指定状态的图书(传入0，查不可借；传入1，查可借)
	 * @param value
	 * @return
	 */
	List<Books> getByStatus(int value);
	/**
	 * 根据作者名字进行模糊查询
	 * @param value
	 * @return
	 */
	List<Books> getByAuthor(String value);
	/**
	 * 根据图书类型查询
	 * @param value
	 * @return
	 */
	List<Books> getByBookType(int value);
	
	List<Books> getByBookNumber(String bookNumber,int status);
	List<Books> getByBookName(String name,int status);
	List<Books> getByAuthor(String value,int status);
	List<Books> getByBookType(int value,int status);
	boolean getByBookNumberExactly(String bookNumber);
	Books getBookByBookNumber(String bookNumber);
	List<Books> query(String value);
}
