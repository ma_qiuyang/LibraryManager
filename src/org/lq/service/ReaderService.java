package org.lq.service;

import java.util.List;

import org.lq.entity.Reader;
/**
 *  读者业务层数据访问接口
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月23日上午11:01:16
 * @package org.lq.service
 */
public interface ReaderService{
	/**
	 * 添加
	 * @param t
	 * @return
	 */
	boolean save(Reader rd);

	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean update(Reader rd);

	/**
	 * 删除
	 * @param t
	 * @return
	 */
	boolean delete(int id);

	/**
	 * 查询全部
	 * @return
	 */
	List<Reader> findAll();

	/**
	 * 通过id查询
	 * @param id
	 * @return
	 */
	Reader getById(int id);
	/**
	 * 根据名称查询
	 * @param name
	 * @return
	 */
	List<Reader> getByName(String name);
	/**
	 * 根据类别查询
	 * @param type
	 * @return
	 */
	List<Reader> getByType(String type);
	/**
	 * 根据用户名和密码登录
	 * @param userName
	 * @param passWord
	 * @return
	 */
	Reader login(String userName,String passWord);
}
