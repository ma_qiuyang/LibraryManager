package org.lq.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.lq.dao.BorrowDao;
import org.lq.entity.Borrow;
import org.lq.util.Instantiation;
import org.lq.util.JDBCUtil;
/**
 * 图书借阅数据访问层接口实现
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.dao.impl
 * @时间 2020年8月18日
 *
 */
public class BorrowDaoImpl implements BorrowDao, Instantiation<Borrow> {

	@Override
	public int save(Borrow t) {
		int num = 0;
		String sql = "insert into borrow (bookId, personId, bookName, personName, bookType, borrowTime, shouldReturnTime, status,actualReturnTime) "
				+ "values"
				+ "(?,?,?,?,?,?,?,?,?)";
		try {
			num = JDBCUtil.executeUpdate(sql, t.getBookId(), t.getPersonId(), t.getBookName(), 
											t.getPersonName(), t.getBookType(), t.getBorrowTime(), 
											t.getShouldReturnTime(), t.getStatus(),t.getActualReturnTime());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	public int update(Borrow t) {
		int num = 0;
		String sql = "update borrow set bookId = ?, personId = ?, bookName = ?, personName = ?, bookType = ?,"
				+ "shouldReturnTime = ?, status = ?, actualReturnTime = ? where id = ?";
				
		try {
			num = JDBCUtil.executeUpdate(sql, t.getBookId(), t.getPersonId(), t.getBookName(), 
											t.getPersonName(), t.getBookType(), t.getShouldReturnTime(), 
											t.getStatus(), t.getActualReturnTime(), t.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	public int delete(int id) {
		int num = 0;
		String sql = "delete * from borrow where id = ?";
		
		try {
			num = JDBCUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	public List<Borrow> findAll() {
		List<Borrow> list = new ArrayList<Borrow>();
		String sql = "select * from borrow";
		try {
			list = JDBCUtil.executeQuery(sql, this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Borrow getById(int id) {
		Borrow borrow = null;
		String sql = "select * from borrow where id = ?";
		
		try {
			borrow = JDBCUtil.findById(sql, this, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return borrow;
	}

	@Override
	public Borrow instance(ResultSet rs) {
		Borrow borrow = null;
		
		try {
			borrow = new Borrow();
			borrow.setId(rs.getInt("id"));
			borrow.setBookId(rs.getInt("bookId")+"");
			borrow.setPersonId(rs.getInt("personId"));
			borrow.setBookName(rs.getString("bookName"));
			borrow.setPersonName(rs.getString("personName"));
			borrow.setBookType(rs.getString("bookType"));
			borrow.setBorrowTime(rs.getTimestamp("borrowTime"));
			borrow.setShouldReturnTime(rs.getTimestamp("shouldReturnTime"));
			borrow.setStatus(rs.getInt("status"));
			borrow.setActualReturnTime(rs.getTimestamp("actualReturnTime"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return borrow;
	}

	@Override
	public List<Borrow> getByPersonId(int personId) {
		List<Borrow> list = new ArrayList<Borrow>();
		String sql = "select * from borrow where personId = ?";
		
		try {
			list = JDBCUtil.executeQuery(sql, this, personId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Borrow> getByStatus(int personId, int status) {
		List<Borrow> list = new ArrayList<Borrow>();
		String sql = "select * from borrow where personId = ? and status = ?";
		
		try {
			list = JDBCUtil.executeQuery(sql, this, personId,status);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	

}
