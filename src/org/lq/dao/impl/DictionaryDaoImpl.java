package org.lq.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.lq.dao.DictionaryDao;
import org.lq.entity.Dictionary;
import org.lq.util.Instantiation;
import org.lq.util.JDBCUtil;

public class DictionaryDaoImpl implements DictionaryDao,Instantiation<Dictionary> {

	@Override
	/**
	 * 添加
	 */
	public int save(Dictionary t) {
		int num= 0;
		String sql = "insert into dictionary (name,typeid) values(?,?)";
		try {
			num = JDBCUtil.executeUpdate(sql, t.getName(),t.getTypeid());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	/**
	 * 修改
	 */
	public int update(Dictionary t) {
		int num = 0;
		String sql = "update dictionary set name=?,typeid=? where id = ?";
		try {
			num = JDBCUtil.executeUpdate(sql, t.getName(),t.getTypeid(),t.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	/**
	 * 删除
	 */
	public int delete(int id) {
		int num = 0;
		String sql = "delete from dictionary where id = ?";
		try {
			num = JDBCUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	/**
	 * 查询所有
	 */
	public List<Dictionary> findAll() {
		List<Dictionary> list = new ArrayList<Dictionary>();
		String sql = "select * from dictionary";
		try {
			list = JDBCUtil.executeQuery(sql, this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	/**
	 * 根据id查询
	 */
	public Dictionary getById(int id) {
		List<Dictionary> findList = new ArrayList<Dictionary>();
		String sql = "select * from dictionary where id = ?";
		try {
			findList = JDBCUtil.executeQuery(sql, this, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return findList.get(0);
	}

	@Override
	/**
	 * 根据typeid查询
	 */
	public List<Dictionary> findByTypeid(int tid) {
		List<Dictionary> findList = new ArrayList<Dictionary>();
		String sql = "select * from dictionary where typeid = ?";
		try {
			findList = JDBCUtil.executeQuery(sql, this, tid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return findList;
	}

	@Override
	/**
	 * 根据名称和typeid查询
	 */
	public Dictionary getByTypeidAndName(String name, int tid) {
		List<Dictionary> findList = new ArrayList<Dictionary>();
		String sql = "select * from dictionary where name = ? and typeid = ?";
		try {
			findList = JDBCUtil.executeQuery(sql, this,name, tid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return findList.get(0);
	}

	@Override
	/**
	 * 查询接口实现
	 */
	public Dictionary instance(ResultSet rs) {
		Dictionary dic = new Dictionary();
		try {
			dic.setId(rs.getInt("id"));
			dic.setName(rs.getString("name"));
			dic.setTypeid(rs.getInt("typeid"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dic;
	}

	@Override
	/**
	 * 根据字典表名称查询
	 */
	public Dictionary getByName(String name) {
		String sql = "select * from dictionary where name = ?";
		List<Dictionary> dic = new ArrayList<Dictionary>();
		try {
			dic = JDBCUtil.executeQuery(sql, this, name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dic.get(0);
	}

}
