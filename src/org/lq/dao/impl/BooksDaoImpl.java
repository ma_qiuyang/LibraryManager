package org.lq.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.lq.dao.BooksDao;
import org.lq.entity.Books;
import org.lq.util.CastUtil;
import org.lq.util.Instantiation;
import org.lq.util.JDBCUtil;

/**
 * 图书类数据访问层接口实现
 * 
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.dao.impl
 * @date 2020年8月18日下午1:12:45
 */
public class BooksDaoImpl implements BooksDao, Instantiation<Books> {
	/**
	 * 添加图书
	 */
	@Override
	public int save(Books t) {
		String sql = "insert into books (bookNumber,bookName,pinYinCode,bookCount,status,author,bookType,maxCount,bookIcon,description,location)"
				+ "values"
				+ "(?,?,?,?,?,?,?,?,?,?,?)";
		int num = 0;
		try {
			num = JDBCUtil.executeUpdate(sql,
					t.getBookNumber(),
					t.getBookName(),
					t.getPinYinCode(),
					t.getBookCount(),
					t.getStatus(),
					t.getAuthor(),
					t.getBookType(),
					t.getMaxCount(),
					t.getBookIcon(),
					t.getDescription(),
					t.getLocation()
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	/**
	 * 修改图书
	 */
	@Override
	public int update(Books t) {
		String sql = "update books set bookNumber= ?,bookName= ?,pinYinCode= ?,bookCount= ?,status= ?,author= ?,"
				+ "bookType= ?,maxCount= ?,bookIcon= ?,description= ?,location=?"
				+ "where  id = ? ";
		int num = 0;
		try {
			num = JDBCUtil.executeUpdate(sql,t.getBookNumber(),
					t.getBookName(),
					t.getPinYinCode(),
					t.getBookCount(),
					t.getStatus(),
					t.getAuthor(),
					t.getBookType(),
					t.getMaxCount(),
					t.getBookIcon(),
					t.getDescription(),
					t.getLocation(),
					t.getId()
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	/**
	 * 删除图书
	 */
	@Override
	public int delete(int id) {
		String sql = "delete from books where id = ?";
		int num = 0;
		try {
			num = JDBCUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	/**
	 * 查询所有图书
	 */
	@Override
	public List<Books> findAll() {
		String sql = "select * from books";
		List<Books> list = new ArrayList<Books>();
		try {
			list = JDBCUtil.executeQuery(sql, this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 通过id获取图书
	 */
	@Override
	public Books getById(int id) {
		String sql = "select * from books where id = ?";
		Books book = new Books();
		try {
			book = JDBCUtil.findById(sql, this, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return book;
	}


	/**
	 * 根据图书编号进行模糊查询
	 * @param id
	 * @return
	 */
	@Override
	public List<Books> getByBookNumber(String bookNumber) {
		String sql = "select * from books where bookNumber like ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, "%"+bookNumber+"%");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 根据图书名称进行模糊查询
	 * @param name
	 * @return
	 */
	@Override
	public List<Books> getByBookName(String name) {
		String sql = "select * from books where bookName like ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, "%"+name+"%");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 根据拼音简码进行模糊查询
	 * @param value
	 * @return
	 */
	@Override
	public List<Books> getByPinYinCode(String value) {
		String sql = "select * from books where pinYinCode like ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, "%"+value+"%");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 查询指定状态的图书(传入0，查不可借；传入1，查可借)
	 * @param value
	 * @return
	 */
	@Override
	public List<Books> getByStatus(int value) {
		String sql = "select * from books where status = ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, value);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 根据作者名字进行模糊查询
	 * @param value
	 * @return
	 */
	@Override
	public List<Books> getByAuthor(String value) {
		String sql = "select * from books where author like ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, "%"+value+"%");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 根据图书类型查询
	 * @param value
	 * @return
	 */
	@Override
	public List<Books> getByBookType(int value) {
		String sql = "select * from books where bookType = ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, value);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	@Override
	public Books instance(ResultSet rs) {
		Books b = new Books();
		try {
				b.setId(rs.getInt("id"));
				b.setBookNumber(rs.getString("bookNumber"));
				b.setBookName(rs.getString("bookName"));
				b.setPinYinCode(rs.getString("pinYinCode"));
				b.setBookCount(rs.getInt("bookCount"));
				b.setStatus(rs.getInt("status"));
				b.setAuthor(rs.getString("author"));
				b.setBookType(rs.getInt("bookType"));
				b.setMaxCount(rs.getInt("maxCount"));
				b.setBookIcon(rs.getString("bookIcon"));
				b.setDescription(rs.getString("description"));
				b.setLocation(rs.getString("location"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return b;
	}
	
	@Override
	public Books getBybookType(int bookType) {
		String sql = "select * from books where bookType = ?";
		List<Books> list = new ArrayList<Books>(0);
		try {
			list = JDBCUtil.executeQuery(sql, this, bookType);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list.get(0);
	}
	
	@Override
	public List<Books> getByBookNumber(String bookNumber, int status) {
		String sql = "select * from books where bookNumber like ? and status = ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, "%"+bookNumber+"%",status);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Books> getByBookName(String name, int status) {
		String sql = "select * from books where bookName like ? and status = ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, "%"+name+"%",status);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Books> getByAuthor(String value, int status) {
		String sql = "select * from books where author like ? and status = ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, "%"+value+"%",status);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Books> getByBookType(int value, int status) {
		String sql = "select * from books where bookType = ? and status = ?"; 
		List<Books> list = new ArrayList<Books>();
		try {
			list =JDBCUtil.executeQuery(sql, this, value,status);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 通过图书编号精准查询
	 */
	@Override
	public Books getByBookNumberExactly(String bookNumber) {
		Books book = new Books();
		String sql = "select * from books where bookNumber = ?"; 
		try {
			book =JDBCUtil.findById(sql, this, CastUtil.castInt(bookNumber));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return book;
	}
}
