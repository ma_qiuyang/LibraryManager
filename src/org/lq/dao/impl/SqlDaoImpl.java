package org.lq.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.lq.dao.SqlDao;
import org.lq.entity.Books;
import org.lq.entity.Sql;
import org.lq.util.Instantiation;
import org.lq.util.JDBCUtil;

public class SqlDaoImpl implements SqlDao ,Instantiation<Sql>{

	@Override
	public int update(Sql s) {
		String sql = "update ssql set status = ? where id = 1";
		int num = 0;
		try {
			num = JDBCUtil.executeUpdate(sql, s.getSatatus());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	public Sql getById(int id) {
		String sql = "select * from ssql where id = ?";
		Sql s = new Sql();
		try {
			s = JDBCUtil.findById(sql, this, 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return s;
	}

	@Override
	public Sql instance(ResultSet rs) {
		Sql s = new Sql();
		try {
			s.setId(rs.getInt("id"));
			s.setSatatus(rs.getInt("status"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return s;
	}

}
