package org.lq.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.lq.dao.OrderListDao;
import org.lq.entity.OrderList;
import org.lq.util.CastUtil;
import org.lq.util.Instantiation;
import org.lq.util.JDBCUtil;
/**
 * 图书入库数据访问层接口
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.dao.impl
 * @时间 2020年8月18日
 *
 */
public class OrderListDaoImpl implements OrderListDao, Instantiation<OrderList> {

	@Override
	public int save(OrderList t) {
		int num = 0;
		String sql = "insert into orderlist (orderNo, bookId, bookName, bookType, number, purchasePrice, totalPrice, supplier, date, note)"
				+ "values"
				+ "(?,?,?,?,?,?,?,?,?,?)";
		
		try {
			num = JDBCUtil.executeUpdate(sql, t.getOrderNo(), t.getBookId(), t.getBookName(), t.getBookType(),
											t.getNumber(), t.getPurchasePrice(), t.getTotalPrice(), t.getSupplier(),
											t.getDate(), t.getNote());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	public int update(OrderList t) {
		int num = 0;
		String sql = "update orderlist set orderNo = ?, bookId = ?, bookName = ?, bookType = ?, number = ?, "
				+ "purchasePrice = ?, totalPrice = ?, supplier = ?, date = ?, note = ? where id = ?";
		
		try {
			num = JDBCUtil.executeUpdate(sql, t.getOrderNo(), t.getBookId(), t.getBookName(), t.getBookType(),
					t.getNumber(), t.getPurchasePrice(), t.getTotalPrice(), t.getSupplier(),
					t.getDate(), t.getNote(), t.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	public int delete(int id) {
		int num = 0;
		String sql = "delete * from orderlist where id = ?";
		try {
			num = JDBCUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	public List<OrderList> findAll() {
		List<OrderList> list = new ArrayList<OrderList>();
		String sql = "select * from orderlist";
		try {
			list = JDBCUtil.executeQuery(sql, this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public OrderList getById(int id) {
		OrderList orderList = null;
		String sql = "select * from orderlist where id = ?";
		
		try {
			orderList = JDBCUtil.findById(sql, this, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return orderList;
	}

	@Override
	public OrderList instance(ResultSet rs) {
		OrderList orderList = null;
		
		try {
			orderList = new OrderList();
			orderList.setId(rs.getInt("id"));
			orderList.setOrderNo(rs.getString("orderNo"));
			orderList.setBookId(rs.getString("bookId"));
			orderList.setBookName(rs.getString("bookName"));
			orderList.setBookType(rs.getString("bookType"));
			orderList.setNumber(rs.getInt("number"));
			orderList.setPurchasePrice(rs.getDouble("purchasePrice"));
			orderList.setTotalPrice(rs.getDouble("totalPrice"));
			orderList.setSupplier(rs.getInt("supplier"));
			orderList.setDate(rs.getTimestamp("date"));
			orderList.setNote(rs.getString("note"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orderList;
	}

	@Override
	public List<OrderList> getOrderByDate(Date start, Date end) {
		List<OrderList> list = new ArrayList<OrderList>();
		String sql = "select * from orderlist where date >= ? and date <= ?";
		
		try {
			list = JDBCUtil.executeQuery(sql, this, start, end);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}



}
