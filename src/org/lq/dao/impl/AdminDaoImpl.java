package org.lq.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.lq.dao.AdminDao;
import org.lq.entity.Admin;
import org.lq.util.Instantiation;
import org.lq.util.JDBCUtil;

/**
 *  管理员数据访问层接口实现
 * @author ASUS
 *
 */

public class AdminDaoImpl implements AdminDao, Instantiation<Admin> {

	/**
	 *  管理员的添加
	 */
	@Override
	public int save(Admin t) {
		String sql = "insert into admin (id,userName,password)"
				+ "values"
				+ "(?,?,?)";
		int num = 0;
		try {
			num = JDBCUtil.executeUpdate(sql,
					t.getId(),
					t.getUserName(),
					t.getPassword()
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}
	
	/**
	 *  管理员的修改
	 */

	@Override
	public int update(Admin t) {
		String sql = "update admin set userName = ?,password = ?"
				+ "where  id = ? ";
		int num = 0;
		try {
			num = JDBCUtil.executeUpdate(sql,t.getUserName(),
					t.getPassword(),
					t.getId()
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}
	
	/**
	 *  管理员的删除
	 */

	@Override
	public int delete(int id) {
		String sql = "delete from admin where id = ?";
		int num = 0;
		try {
			num = JDBCUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}
	
	/**
	 *  管理员的查看全部
	 */

	@Override
	public List<Admin> findAll() {
		String sql = "select * from admin";
		List<Admin> list = new ArrayList<Admin>();
		try {
			list = JDBCUtil.executeQuery(sql, this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 *  根据id查询
	 */

	@Override
	public Admin getById(int id) {
		String sql = "select * from admin where id = ?";
		List<Admin> list = new ArrayList<Admin>();
		try {
			list = JDBCUtil.executeQuery(sql, this, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list.get(0);
	}
	
	@Override
	public Admin instance(ResultSet rs) {
		Admin b = new Admin();
		try {
				b.setId(rs.getInt("id"));
				b.setUserName(rs.getString("userName"));
				b.setPassword(rs.getString("password"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return b;
	}

	@Override
	//登录操作
	public Admin getByUser(String username, String password) {
		List<Admin> list = new ArrayList<Admin>();
		String sql = "select * from admin where username=? and password=?";
		try {
			list = JDBCUtil.executeQuery(sql, this, username,password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list.get(0);
	}

}
