package org.lq.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.lq.dao.ReaderDao;
import org.lq.entity.Reader;
import org.lq.util.Instantiation;
import org.lq.util.JDBCUtil;
/**
 * 读者数据访问层接口实现
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月19日下午7:03:42
 * @package org.lq.dao.impl
 */
public class ReaderDaoImpl implements ReaderDao,Instantiation<Reader> {

	@Override
	/**
	 * 添加读者
	 */
	public int save(Reader t) {
		int num = 0;
		String sql = "insert into reader values(null,?,?,?,?,?,?,?,?,?)";
		try {
			num = JDBCUtil.executeUpdate(sql, t.getName(),t.getGender(),t.getUserName(),t.getPassWord(),t.getBorrowTime(),
					t.getBorrowStart(),t.getType(),t.getNum(),t.getNotreturnnum());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	/**
	 * 修改读者
	 */
	public int update(Reader t) {
		int num = 0;
		String sql = "update reader set name=?,gender=?,username=?,password=?,borrowtime=?,"
				+ "borrowstart=?,type=?,num=?,nreturnnum=? where id=?";
		try {
			num = JDBCUtil.executeUpdate(sql, t.getName(),t.getGender(),t.getUserName(),t.getPassWord(),t.getBorrowTime(),
						t.getBorrowStart(),t.getType(),t.getNum(),t.getNotreturnnum(),t.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	/**
	 * 删除读者
	 */
	public int delete(int id) {
		int num = 0;
		String sql = "delete from reader where id = ?";
		try {
			num = JDBCUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	/**
	 * 查询所有
	 */
	public List<Reader> findAll() {
		List<Reader> list = new ArrayList<Reader>();
		String sql = "select * from reader";
		try {
			list = JDBCUtil.executeQuery(sql, this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	/**
	 * 根据id查询
	 */
	public Reader getById(int id) {
		List<Reader> findList = new ArrayList<Reader>();
		String sql = "select * from reader where id = ?";
		try {
			findList = JDBCUtil.executeQuery(sql, this, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return findList.get(0);
	}

	@Override
	/**
	 * 根据用户名和密码查询
	 */
	public Reader login(String userName, String passWord) {
		List<Reader> list = new ArrayList<Reader>();
		String sql = "select * from reader where username=? and password=?";
		try {
			list = JDBCUtil.executeQuery(sql, this, userName,passWord);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list.get(0);
	}

	@Override
	/**
	 * 查询接口实现
	 */
	public Reader instance(ResultSet rs) {
		Reader reader = new Reader();
		try {
			reader.setId(rs.getInt("id"));
			reader.setName(rs.getString("name"));
			reader.setGender(rs.getString("gender"));
			reader.setUserName(rs.getString("username"));
			reader.setPassWord(rs.getString("password"));
			reader.setBorrowTime(rs.getInt("borrowtime"));
			reader.setBorrowStart(rs.getTimestamp("borrowstart"));
			reader.setType(rs.getString("type"));
			reader.setNum(rs.getInt("num"));
			reader.setNotreturnnum(rs.getInt("nreturnnum"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reader;
	}

	@Override
	/**
	 * 根据名称查询
	 */
	public List<Reader> getByName(String name) {
		String sql = "select * from reader where name like ?";
		List<Reader> byName = new ArrayList<Reader>();
		try {
			byName = JDBCUtil.executeQuery(sql, this, "%"+name+"%");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return byName;
	}

	@Override
	/**
	 * 根据类别查询
	 */
	public List<Reader> getByType(String type) {
		String sql = "select * from reader where type=?";
		List<Reader> byType = new ArrayList<Reader>();
		try {
			byType = JDBCUtil.executeQuery(sql, this, type);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return byType;
	}

}
