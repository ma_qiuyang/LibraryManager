package org.lq.dao;

import java.util.Date;
import java.util.List;

import org.lq.entity.OrderList;

/**
 * 图书入库数据访问层接口
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.dao
 * @时间 2020年8月18日
 *
 */
public interface OrderListDao extends BaseDao<OrderList> {
	/**
	 * 根据时间查找
	 * @param start
	 * @param end
	 * @return
	 */
	List<OrderList> getOrderByDate(Date start,Date end);
}
