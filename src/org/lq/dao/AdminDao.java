package org.lq.dao;

import org.lq.entity.Admin;

/**
 *  管理员数据访问层接口
 * @author 付金晨
 *
 */

public interface AdminDao extends BaseDao<Admin> {
	
	Admin getByUser(String username,String password);
	
}
