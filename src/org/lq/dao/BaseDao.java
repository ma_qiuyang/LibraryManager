package org.lq.dao;

import java.util.List;

/**
 * 通用泛型接口
 * 
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.dao.impl
 * @时间 2020年8月18日
 *
 */
public interface BaseDao<T> {
	/**
	 * 添加
	 * @param t
	 * @return
	 */
	int save(T t);

	/**
	 * 修改
	 * @param t
	 * @return
	 */
	int update(T t);

	/**
	 * 删除
	 * @param t
	 * @return
	 */
	int delete(int id);

	/**
	 * 查询全部
	 * @return
	 */
	List<T> findAll();

	/**
	 * 通过id查询
	 * @param id
	 * @return
	 */
	T getById(int id);
}
