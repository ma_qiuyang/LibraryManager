package org.lq.dao;

import org.lq.entity.Sql;

public interface SqlDao {
	//修改自动备份状态
	int update(Sql s);
	//获取对象
	Sql getById(int id);
}
