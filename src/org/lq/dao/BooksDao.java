package org.lq.dao;

import java.util.List;

import org.lq.entity.Books;
/**
 * 图书类数据访问层接口
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.dao
 * @date 2020年8月18日下午1:10:21
 */
public interface BooksDao extends BaseDao<Books> {
	/**
	 * 根据图书编号进行模糊查询
	 * @param id
	 * @return
	 */
	List<Books> getByBookNumber(String bookNumber);
	List<Books> getByBookNumber(String bookNumber,int status);
	Books getByBookNumberExactly(String bookNumber);
	/**
	 * 根据图书名称进行模糊查询
	 * @param name
	 * @return
	 */
	List<Books> getByBookName(String name);
	List<Books> getByBookName(String name,int status);
	/**
	 * 根据拼音简码进行模糊查询
	 * @param value
	 * @return
	 */
	List<Books> getByPinYinCode(String value);
	/**
	 * 查询指定状态的图书(传入0，查不可借；传入1，查可借)
	 * @param value
	 * @return
	 */
	List<Books> getByStatus(int value);
	/**
	 * 根据作者名字进行模糊查询
	 * @param value
	 * @return
	 */
	List<Books> getByAuthor(String value);
	List<Books> getByAuthor(String value,int status);
	/**
	 * 根据图书类型查询
	 * @param value
	 * @return
	 */
	List<Books> getByBookType(int value);
	List<Books> getByBookType(int value,int status);
	/**
	 * 根据图书类型查询
	 * @param bookType
	 * @return
	 */
	Books getBybookType(int bookType);
}
