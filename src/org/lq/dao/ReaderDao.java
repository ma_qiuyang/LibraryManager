package org.lq.dao;

import java.util.List;

import org.lq.entity.Reader;
/**
 * 读者数据访问层接口
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月19日下午7:03:50
 * @package org.lq.dao
 */
public interface ReaderDao extends BaseDao<Reader>{
	/**
	 * 根据用户名和密码登录
	 * @param userName
	 * @param passWord
	 * @return
	 */
	Reader login(String userName,String passWord);
	/**
	 * 根据名称查询
	 * @param name
	 * @return
	 */
	List<Reader> getByName(String name);
	/**
	 * 根据类别查询
	 * @param type
	 * @return
	 */
	List<Reader> getByType(String type);
}
