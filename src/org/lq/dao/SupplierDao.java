package org.lq.dao;

import java.util.List;

import org.lq.entity.Supplier;
/**
 * 供应商接口
 * @author 付金晨
 */
public interface SupplierDao extends BaseDao<Supplier> {
	
	
	/**
	 *	通过名称模糊查询
	 * @param name
	 * @return
	 */
	List<Supplier> likeSupplierByName(String name);
	
	

}
