package org.lq.dao;

import java.util.List;

import org.lq.entity.Dictionary;
/**
 * 字典数据层接口
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月23日上午10:22:46
 * @package org.lq.dao
 */
public interface DictionaryDao extends BaseDao<Dictionary>{
	/**
	 * 根据typeid查询
	 * @param tid
	 * @return
	 */
	List<Dictionary> findByTypeid(int tid);
	/**
	 * 根据名称和typeid查询
	 * @param name
	 * @param tid
	 * @return
	 */
	Dictionary getByTypeidAndName(String name,int tid);
	/**
	 * 根据名称查询
	 * @param name
	 * @return
	 */
	Dictionary getByName(String name);
}
