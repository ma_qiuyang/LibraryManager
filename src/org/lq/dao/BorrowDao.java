package org.lq.dao;

import java.util.List;

import org.lq.entity.Borrow;
/**
 * 图书借阅数据访问层接口
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.dao
 * @时间 2020年8月18日
 *
 */
public interface BorrowDao extends BaseDao<Borrow> {
	/**
	 * 根据图书状态(已还或者未还)获取读者的借阅历史
	 * @param status
	 * @return
	 */
	List<Borrow> getByStatus(int personId, int status);
	/**
	 * 根据借阅人查询借阅历史
	 * @param personId
	 * @return
	 */
	List<Borrow> getByPersonId(int personId);
}
