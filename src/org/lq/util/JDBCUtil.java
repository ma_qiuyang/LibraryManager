package org.lq.util;
/**
 * 数据库连接工具类
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.util
 * @时间 2020年8月18日
 *
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JDBCUtil {
	private static final String USER = "root";
	private static final String PASSWORD = "root";
	private static final String URL = "jdbc:mysql://localhost:3306/librarymanager";
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	
	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	/**
	 * 通用添加删除修改方法
	 * 
	 * @param sql   SQL语句
	 * @param param 需要用到的参数
	 * @return 受影响行数
	 * @throws SQLException
	 */
	public static int executeUpdate(String sql, Object... param) throws SQLException {
		int num = 0;
		try (
				Connection conn = getConnection(); 
				PreparedStatement ps = conn.prepareStatement(sql);
			) {
			if (param != null && param.length > 0) {
				for (int i = 0; i < param.length; i++) {
					ps.setObject(i + 1, param[i]);
				}
			}
			num = ps.executeUpdate();
		} catch (SQLException e) {
			throw e;
		}
		return num;
	}
	
	/**
	 * 通用查询方法
	 * 
	 * @param <T>    需要返回的实例对象(泛型中的类型)
	 * @param sql    SQL语句
	 * @param in     对实例对象创建的结构
	 * @param params 需要用到的参数
	 * @return list
	 * @throws SQLException
	 */
	public static <T> List<T> executeQuery(String sql, Instantiation<T> in, Object... params) throws SQLException {
		List<T> list = new ArrayList<T>();
		ResultSet rs = null;
		try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql);) {
			if (params != null && params.length > 0) {
				for (int i = 0; i < params.length; i++) {
					ps.setObject(i + 1, params[i]);
				}
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(in.instance(rs));
			}

		} catch (SQLException e) {
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
				rs = null;
			}
		}
		return list;
	}
	/**
	 * 根据ID查找
	 * @param <T>
	 * @param sql
	 * @param in
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static <T> T findById(String sql, Instantiation<T> in, int id) throws SQLException {
		ResultSet rs = null;
		T t = null;
		try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql);) {
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				t = in.instance(rs);
			}
		} catch (SQLException e) {
			throw e;
		}
		return t;
	}
	/**
	 * 关闭
	 * @param rs
	 * @param ps
	 * @param conn
	 */
	public static void closeAll(ResultSet rs,PreparedStatement ps,Connection conn) {
		try {
			if(rs != null) {
				rs.close();
				rs = null;
			}
			if(ps != null) {
				ps.close();
				ps = null;
			}
			if(conn!=null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
