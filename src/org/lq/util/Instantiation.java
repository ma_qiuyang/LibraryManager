package org.lq.util;

import java.sql.ResultSet;
/**
 * 查询接口
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.util
 * @时间 2020年8月18日
 *
 * @param <T>
 */
public interface Instantiation<T> {
	/**
	 * 返回对象
	 * @param rs 数据库返回的结果
	 * @return 泛型类型的实例化对象
	 */
	T instance(ResultSet rs);
}
