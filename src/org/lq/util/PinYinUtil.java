package org.lq.util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class PinYinUtil {
	
	/**
	 * 	根据字符串进行拼音转换
	 * @param str : 每个汉字的首拼音并大写
	 * @return
	 */
	public static String getFirstPY(String str) {
		StringBuilder sbu = new StringBuilder();//存储完成的字符串
		HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();//创建格式化器
		format.setCaseType(HanyuPinyinCaseType.UPPERCASE);//转为大写
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);//去除声调
		
		for (int i = 0; i < str.length(); i++) {
			try {
				//把单个字符根据指定格式转为char拼音数组（包含多个读音）
				String py [] = PinyinHelper.toHanyuPinyinStringArray(str.charAt(i),format);
				String p = py[0];//获取第一个读音
				sbu.append(p.charAt(0));//获取读音的第一个字母，并追加到sbu
			} catch (Exception e) {
				sbu.append(str.charAt(i));//出现异常，代表不是汉字，直接进行拼接
			}
		}
		
		return sbu.toString();
	}

}
