package org.lq.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 类型转换工具类
 * 
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.util
 * @时间 2020年8月19日
 *
 */
public class CastUtil {
	/**
	 * 年-月-日
	 */
	public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
	/**
	 * 年月日
	 */
	public static final String DATE_PATTERN = "yyyyMMdd";
	/**
	 * 年-月-日 时:分:秒
	 */
	public static final String TIME_DATE_PATTERN_1 = "yyyy-MM-dd hh:mm:ss";
	/**
	 * 年-月-日-时-分-秒
	 */
	public static final String TIME_DATE_PATERN_2 = "yyyy-MM-dd-HH-mm-ss";
	/**
	 * 年月日 时分秒
	 */
	public static final String TIME_DATE_PATERN_3 = "yyyyMMddhhmmss";
	public static final String TIME_DATE_PATERN_4 = "yyyyMMddHHmmss";
	
	/**
	 * 将java.util.date格式化为默认格式(年-月-日)的字符串
	 * @param date
	 * @return
	 */
	public static String format(Date date) {
		return date == null ? "" : format(date,DEFAULT_DATE_PATTERN);
	}
	/**
	 * 将java.util.date格式化为指定格式的字符串
	 * @param date 时间
	 * @param pattern 格式化方式
	 * @return 格式化结果
	 */
	public static String format(Date date, String pattern) {
		return date == null ? "" : new SimpleDateFormat(pattern).format(date);
	}
	/**
	 * 将字符串格式化为指定格式的java.util.date
	 * @param strDate 需要格式化的字符串
	 * @param pattern 格式化方式
	 * @return 格式化结果
	 * @throws ParseException
	 */
	public static Date parseUtilDate(String strDate, String pattern) throws ParseException {
		return strDate == null && "".equals(strDate) ? null : new SimpleDateFormat(pattern).parse(strDate);
	}
	/**
	 * 将字符串格式化为指定格式的java.sql.date
	 * @param strDate 需要格式化的字符串
	 * @param pattern 格式化方式
	 * @return 格式化结果
	 * @throws ParseException
	 */
	public static java.sql.Date parseSqlDate(String strDate, String pattern) throws ParseException {
		//java.sql.date new的时候传参为毫秒数(long类型),把时间先转化为java.util.date,再getTime变为毫秒数
		return strDate == null && "".equals(strDate) ? null : new java.sql.Date(parseUtilDate(strDate, pattern).getTime());
	}
	public static java.sql.Date parseUtilDateToSqlDate(Date date){
		if (date == null) {
			date = new Date();
		}
		return new java.sql.Date(date.getTime());
	}
	/**
	 * 将其他任意类型转换为字符串,若为空,则返回""
	 * @param obj 要转换的值
	 * @return
	 */
	public static String castString(Object obj) {
		return obj == null ? "" : String.valueOf(obj);
	}
	/**
	 * 将其他任意类型转换为字符串,若为空,则返回默认值
	 * @param obj 要转换的值
	 * @param defaultValue 默认值
	 * @return
	 */
	public static String castString(Object obj, String defaultValue) {
		return obj == null ? defaultValue : String.valueOf(obj);
	}
	/**
	 * 将其他任意类型转换为double类型,若为空或者字母,则返回0
	 * @param obj 要转换的值
	 * @return
	 */
	public static double castDouble(Object obj) {
		return castDouble(obj, 0);
	}
	/**
	 * 将其他任意类型转换为double类型,若为空或者字母,则返回默认值
	 * @param obj 要转换的值
	 * @param defaultValue 默认值
	 * @return
	 */
	public static double castDouble(Object obj, double defaultValue) {
		double value = defaultValue;
		if (obj != null) {
			String strValue = String.valueOf(obj);
			try {
				if (!"".equals(strValue)) {
					//如果参数为字母,则不为空,也不是空字符串,但是转换为发生NumberFormatException异常
					value = Double.parseDouble(strValue);
				}
			} catch (NumberFormatException e) {
				value = defaultValue;
				e.printStackTrace();
			}
		}
		return value;
	}
	/**
	 * 将其他任意类型转换为int类型,若为空或者字母,则返回0
	 * @param obj
	 * @return
	 */
	public static int castInt(Object obj) {
		return castInt(obj, 0);
	}
	/**
	 * 将其他任意类型转换为int类型,若为空或者字母,则返回默认值
	 * @param obj 要转换的值
	 * @param defaultValue 默认值
	 * @return
	 */
	public static int castInt(Object obj, int defaultValue) {
		int value = defaultValue;
		if (obj != null) {
			String strValue = String.valueOf(obj);
			try {
				if (!"".equals(strValue)) {
					//如果参数为字母,则不为空,也不是空字符串,但是转换为发生NumberFormatException异常
					value = Integer.valueOf(strValue);
				}
			} catch (NumberFormatException e) {
				value = defaultValue;
				e.printStackTrace();
			}
		}
		return value;
	}
	

}
