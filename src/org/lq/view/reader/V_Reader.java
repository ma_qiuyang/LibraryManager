package org.lq.view.reader;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.lq.entity.Reader;
import org.lq.service.ReaderService;
import org.lq.service.impl.ReaderServiceImpl;
import org.lq.util.CastUtil;

public class V_Reader extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static JTable table;
	private ReaderService rs = new ReaderServiceImpl();
	private JTextField text_cx;
	private int rid;//独有的方法,用来获取id
	/**
	 * 获取选中读者的id
	 * @return
	 */
	public int getRid() {
		return rid;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_Reader frame = new V_Reader();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_Reader() {
		setTitle("读者管理");
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_Reader.class.getResource("/images/读者管理.png")));
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 623, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 10, 596, 87);
		contentPane.add(panel);
		panel.setLayout(null);

		text_cx = new JTextField();
		text_cx.setBounds(121, 21, 66, 21);
		panel.add(text_cx);
		text_cx.setColumns(10);

		JComboBox cx_type = new JComboBox();
		cx_type.setModel(new DefaultComboBoxModel(new String[] {"---请选择---", "编号查询", "姓名查询", "类别查询"}));
		cx_type.setBounds(10, 20, 101, 23);
		panel.add(cx_type);

		JButton btnNewButton = new JButton("查询读者");
		btnNewButton.setIcon(new ImageIcon(V_Reader.class.getResource("/images/查询.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = (int) cx_type.getSelectedIndex();
				String text = text_cx.getText();
				//根据id查询
				if(i == 1){
					int id = CastUtil.castInt(text);
					try {
						Reader byId = rs.getById(id);
						List<Reader> list = new ArrayList<Reader>();
						list.add(byId);
						initTable(list);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(V_Reader.this, "该读者不存在呦,亲","查询",JOptionPane.ERROR_MESSAGE);
					}
				}
				//根据名称查询
				else if(i == 2) {
					List<Reader> byName = rs.getByName(text);
					initTable(byName);
				}
				//根据类别查询
				else {
					List<Reader> byType = rs.getByType(text);
					initTable(byType);
				}
			}
		});
		btnNewButton.setBounds(205, 20, 112, 23);
		panel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("添加读者");
		btnNewButton_1.setIcon(new ImageIcon(V_Reader.class.getResource("/images/add.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//调用添加页面
				V_ReaderAdd add = new V_ReaderAdd();
				add.setBounds(V_Reader.this.getX()+100, V_Reader.this.getY(), add.getWidth(), add.getHeight());
				add.setVisible(true);
				add.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						//当添加窗体关闭的时候,调用表格初始化方法
						initTable();
					}
					@Override
					public void windowClosing(WindowEvent e) {
					}
				});
			}
		});
		btnNewButton_1.setBounds(342, 20, 112, 23);
		panel.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("刷新表格");
		btnNewButton_2.setIcon(new ImageIcon(V_Reader.class.getResource("/images/刷新.png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cx_type.setSelectedIndex(0);//把下拉列表初始为0
				text_cx.setText("");//把搜索框赋值为0
				initTable();
			}
		});
		btnNewButton_2.setBounds(475, 20, 111, 23);
		panel.add(btnNewButton_2);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 107, 596, 380);
		contentPane.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"\u7F16\u53F7", "\u59D3\u540D", "\u6027\u522B", "\u7528\u6237\u540D", "\u5BC6\u7801", "\u53EF\u501F\u65F6\u957F", "\u7C7B\u522B", "\u53EF\u501F\u6570\u91CF", "\u672A\u8FD8\u6570\u91CF"
			}
		));
		scrollPane.setViewportView(table);

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(table, popupMenu);

		JMenuItem update = new JMenuItem("修改"); 
		update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();//获取选中的行
				rid = CastUtil.castInt(table.getValueAt(index, 0));//获取选中行的第一列
				//调用添加页面,并且回填数据
				V_ReaderAdd add = new V_ReaderAdd(rid);
				add.setBounds(V_Reader.this.getX()+100, V_Reader.this.getY(), add.getWidth(), add.getHeight());
				add.setVisible(true);
				add.huitian();//调用回填数据的方法
				add.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						//当添加窗体关闭的时候,调用表格初始化方法
						initTable();
					}
					@Override
					public void windowClosing(WindowEvent e) {
					}
				});
			}
		});
		popupMenu.add(update);

		JMenuItem delete = new JMenuItem("删除");
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();//获取选中的行
				int id = CastUtil.castInt(table.getValueAt(index, 0));//获取选中行的第一列
				Reader byId = rs.getById(id);//根据Id查询返回对象
				if(byId.getNotreturnnum() == 0) {
					int n = JOptionPane.showConfirmDialog(V_Reader.this, "你确定删除吗?","删除",JOptionPane.OK_CANCEL_OPTION);
					if(n == 0) {
						showMessage(rs.delete(id), "删除");
						initTable(rs.findAll());//初始化表格
					}
				}
				else {
					JOptionPane.showMessageDialog(V_Reader.this, "还有图书为归还,禁止删除","删除",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		popupMenu.add(delete);
		//初始化表格
		List<Reader> list = rs.findAll();
		initTable(list);
	}
	/**
	 * 消息提示
	 * @param bool
	 * @param msg
	 */
	public void showMessage(boolean bool,String msg) {
		if(bool) {
			JOptionPane.showMessageDialog(V_Reader.this, msg+"成功!",msg,JOptionPane.INFORMATION_MESSAGE);
		}else {
			JOptionPane.showMessageDialog(V_Reader.this, msg+"失败!",msg,JOptionPane.ERROR_MESSAGE);
		}
	}
	/**
	 * 初始化表格
	 * @param list
	 */
	public void initTable(List<Reader> list) {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int count = dtm.getRowCount();//获取表中的总行数
		for (int i = 0; i < count; i++) {
			dtm.removeRow(0);//清除表中的所有元素
		}
		for (Reader reader : list) {
			dtm.addRow(new Object[] {
					reader.getId(),
					reader.getName(),
					reader.getGender(),
					reader.getUserName(),
					reader.getPassWord(),
					reader.getBorrowTime(),
					reader.getType(),
					reader.getNum(),
					reader.getNotreturnnum()
			});
		}
	}
	/**
	 * 初始化表格
	 */
	public void initTable() {
		initTable(rs.findAll());
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				int index= table.rowAtPoint(e.getPoint());//获取点击的行的座标
				table.setRowSelectionInterval(index, index);//选中点击的行
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
