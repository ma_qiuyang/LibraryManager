package org.lq.view.reader;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import javax.swing.UIManager;
/**
 * 读者主界面
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.view.reader
 * @date 2020年9月4日下午6:27:12
 */
public class V_readerMain extends JFrame {

	private JPanel contentPane;
	private JTabbedPane tabbedPane;
	private static int id;
	public int getId() {
		return this.id;
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_readerMain frame = new V_readerMain(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_readerMain(int id) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_readerMain.class.getResource("/images/用户名.png")));
		setTitle("用户界面");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1254, 692);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		BtnActionListener btnClick = new BtnActionListener(tabbedPane,id);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JButton btnNewButton = new JButton("查询图书");
		btnNewButton.setIcon(new ImageIcon(V_readerMain.class.getResource("/images/查询.png")));
		btnNewButton.addActionListener(btnClick);
		menuBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("借阅图书");
		btnNewButton_1.setIcon(new ImageIcon(V_readerMain.class.getResource("/images/审批借阅.png")));
		btnNewButton_1.addActionListener(btnClick);
		menuBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("归还图书");
		btnNewButton_2.setIcon(new ImageIcon(V_readerMain.class.getResource("/images/图书归还.png")));
		btnNewButton_2.addActionListener(btnClick);
		menuBar.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("修改密码");
		btnNewButton_3.setBackground(UIManager.getColor("Button.darkShadow"));
		btnNewButton_3.setIcon(new ImageIcon(V_readerMain.class.getResource("/images/修改.png")));
		btnNewButton_3.addActionListener(btnClick);
		menuBar.add(btnNewButton_3);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(V_readerMain.class.getResource("/images/nature-5133834_1920.jpg")));
		tabbedPane.addTab("", null, lblNewLabel, null);
		
		
	
		
		
	}
}
