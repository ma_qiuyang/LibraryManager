package org.lq.view.reader;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.lq.entity.Reader;
import org.lq.service.ReaderService;
import org.lq.service.impl.ReaderServiceImpl;
import org.lq.util.CastUtil;

public class V_ReaderAdd extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField t_id;
	private JTextField name;
	private JTextField username;
	private JPasswordField password;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup2 = new ButtonGroup();
	private ReaderService rs = new ReaderServiceImpl();
	private JRadioButton gender_man;
	private JRadioButton gender_woman;
	private JRadioButton teacher;
	private JRadioButton student;
	private int id;
	public V_ReaderAdd(int id)
	{
		this();
		this.id = id;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_ReaderAdd frame = new V_ReaderAdd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_ReaderAdd() {
		setTitle("添加读者");
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_ReaderAdd.class.getResource("/images/add.png")));

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 265, 428);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("编 号 :");
		lblNewLabel.setBounds(15, 25, 58, 15);
		contentPane.add(lblNewLabel);

		t_id = new JTextField();
		t_id.setEnabled(false);
		t_id.setBounds(100, 22, 66, 21);
		contentPane.add(t_id);
		t_id.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("姓 名 :");
		lblNewLabel_1.setBounds(15, 74, 58, 15);
		contentPane.add(lblNewLabel_1);

		name = new JTextField();
		name.setBounds(100, 71, 94, 21);
		contentPane.add(name);
		name.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("性 别 : ");
		lblNewLabel_2.setBounds(15, 120, 58, 15);
		contentPane.add(lblNewLabel_2);

		gender_man = new JRadioButton("男");
		buttonGroup.add(gender_man);
		gender_man.setBounds(100, 116, 42, 23);
		contentPane.add(gender_man);

		gender_woman = new JRadioButton("女");
		buttonGroup.add(gender_woman);
		gender_woman.setBounds(150, 116, 42, 23);
		contentPane.add(gender_woman);

		JLabel lblNewLabel_3 = new JLabel("用户名 :");
		lblNewLabel_3.setBounds(15, 169, 58, 15);
		contentPane.add(lblNewLabel_3);

		username = new JTextField();
		username.setBounds(100, 166, 92, 21);
		contentPane.add(username);
		username.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("密 码 :");
		lblNewLabel_4.setBounds(15, 219, 58, 15);
		contentPane.add(lblNewLabel_4);

		password = new JPasswordField();
		password.setBounds(100, 216, 94, 21);
		contentPane.add(password);

		JLabel lblNewLabel_5 = new JLabel("读者类别:");
		lblNewLabel_5.setBounds(10, 276, 58, 15);
		contentPane.add(lblNewLabel_5);

		teacher = new JRadioButton("老师");
		buttonGroup2.add(teacher);
		teacher.setBounds(100, 272, 58, 23);
		contentPane.add(teacher);

		student = new JRadioButton("学生");
		student.setBounds(164, 272, 49, 23);
		buttonGroup2.add(student);
		contentPane.add(student);

		JButton btnNewButton = new JButton("保存");
		btnNewButton.setIcon(new ImageIcon(V_ReaderAdd.class.getResource("/images/save.png")));
		btnNewButton.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				//添加读者
				Reader r = new Reader();
				if(name.getText().equals("") || username.getText().equals("") || password.getText().equals("")){
					JOptionPane.showMessageDialog(V_ReaderAdd.this, "选项不能为空！","错误",JOptionPane.ERROR_MESSAGE);
				}else {
					r.setName(name.getText());
					r.setGender(gender_man.isSelected() ? "男" : "女");
					r.setUserName(username.getText());
					r.setPassWord(password.getText());
					String time = student.isSelected() ? "20" : "30";
					r.setBorrowTime(CastUtil.castInt(time));
					r.setBorrowStart(null);
					r.setType(teacher.isSelected() ? "老师" : "学生");
					String num = teacher.isSelected() ? "5" : "3";
					r.setNum(CastUtil.castInt(num));
					r.setNotreturnnum(0);
					int id = CastUtil.castInt(t_id.getText());
					if(id == 0) {
						if(rs.save(r)) {
							JOptionPane.showMessageDialog(V_ReaderAdd.this, "恭喜添加成功！","提示",JOptionPane.INFORMATION_MESSAGE);
							dispose();
						}
						else{
							JOptionPane.showMessageDialog(V_ReaderAdd.this, "添加失败！","错误",JOptionPane.ERROR_MESSAGE);
						}
					}else {
						r.setId(id);
						if(rs.update(r)) {
							JOptionPane.showMessageDialog(V_ReaderAdd.this, "恭喜修改成功！","提示",JOptionPane.INFORMATION_MESSAGE);
							dispose();
						}
						else{
							JOptionPane.showMessageDialog(V_ReaderAdd.this, "修改失败！","错误",JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		btnNewButton.setBounds(80, 345, 97, 23);
		contentPane.add(btnNewButton);
	}
	/**
	 * 回填
	 */
	public void huitian() {
		Reader red = rs.getById(id);
		t_id.setText(red.getId()+"");
		name.setText(red.getName());
		if(red.getGender().equals("男")) {
			gender_man.setSelected(true);
		}else {
			gender_woman.setSelected(true);
		}
		username.setText(red.getUserName());
		password.setText(red.getPassWord());
		if(red.getType().equals("老师")) {
			teacher.setSelected(true);
		}else {
			student.setSelected(true);
		}
	}

}
