package org.lq.view.reader;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import org.lq.entity.Reader;
import org.lq.service.ReaderService;
import org.lq.service.impl.ReaderServiceImpl;
import org.lq.view.enroll.V_Enroll;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class V_UpdatePassword extends JFrame {

	private JPanel contentPane;
	private JPasswordField oldpsd;
	private JPasswordField newpsd;
	private JPasswordField qrpsd;
	private ReaderService rs = new ReaderServiceImpl();
	private static int id;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_UpdatePassword frame = new V_UpdatePassword(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_UpdatePassword(int id) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 444, 402);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("原始密码:");
		lblNewLabel.setBounds(99, 50, 58, 15);
		contentPane.add(lblNewLabel);

		oldpsd = new JPasswordField();
		oldpsd.setBounds(191, 47, 157, 21);
		contentPane.add(oldpsd);
		oldpsd.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("新的密码:");
		lblNewLabel_1.setBounds(99, 111, 58, 15);
		contentPane.add(lblNewLabel_1);

		newpsd = new JPasswordField();
		newpsd.setBounds(191, 108, 157, 21);
		contentPane.add(newpsd);
		newpsd.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("确认密码:");
		lblNewLabel_2.setBounds(99, 173, 58, 15);
		contentPane.add(lblNewLabel_2);

		qrpsd = new JPasswordField();
		qrpsd.setBounds(191, 170, 157, 21);
		contentPane.add(qrpsd);
		qrpsd.setColumns(10);

		JButton btnNewButton = new JButton("确定");
		btnNewButton.setIcon(new ImageIcon(V_UpdatePassword.class.getResource("/images/tick.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_Enroll enroll = new V_Enroll();
//				id = enroll.getId();
				Reader red = rs.getById(id);//这里面需要改,这里面的0需要改为从登录界面获取到的对象的id
				String password = red.getPassWord();//从对象中得到的密码
				String oldpsd1 = oldpsd.getText();
				String newpsd1 = newpsd.getText();
				String qrpsd1 = qrpsd.getText();
				if(oldpsd1.equals("") || newpsd1.equals("") || qrpsd1.equals("")) {
					JOptionPane.showMessageDialog(V_UpdatePassword.this, "输入的值不能为空","",JOptionPane.ERROR_MESSAGE);
				}else {
					if(password.equals(oldpsd1)) {
						if(!oldpsd1.equals(newpsd1)) {
							red.setPassWord(newpsd1);
							if(rs.update(red)) {
								JOptionPane.showMessageDialog(V_UpdatePassword.this, "密码修改成功!","",JOptionPane.INFORMATION_MESSAGE);
								oldpsd.setText("");
								newpsd.setText("");
								qrpsd.setText("");
								dispose();
							}else {
								JOptionPane.showMessageDialog(V_UpdatePassword.this, "密码修改失败","",JOptionPane.ERROR_MESSAGE);
							}
						}else {
							JOptionPane.showMessageDialog(V_UpdatePassword.this, "新密码不能和原始密码一样","",JOptionPane.ERROR_MESSAGE);
						}
					}else {
						JOptionPane.showMessageDialog(V_UpdatePassword.this, "输入的原始密码有误","",JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnNewButton.setBounds(99, 262, 97, 23);
		contentPane.add(btnNewButton);

		JLabel lbl_message = new JLabel("");
		lbl_message.setForeground(Color.RED);
		lbl_message.setBounds(153, 10, 195, 15);
		contentPane.add(lbl_message);
		
		JButton btnNewButton_1 = new JButton("初始化密码");
		btnNewButton_1.setIcon(new ImageIcon(V_UpdatePassword.class.getResource("/images/修改.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Reader byId = rs.getById(id);
				byId.setPassWord("123456");
				if(rs.update(byId)) {
					JOptionPane.showMessageDialog(V_UpdatePassword.this, "密码123456初始化成功!","",JOptionPane.INFORMATION_MESSAGE);
				}else {
					JOptionPane.showMessageDialog(V_UpdatePassword.this, "密码初始化失败","",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton_1.setBounds(248, 262, 113, 23);
		contentPane.add(btnNewButton_1);
	}
}
