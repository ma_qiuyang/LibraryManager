package org.lq.view.reader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTabbedPane;

import org.lq.view.books.V_booksUser;
import org.lq.view.borrow.V_Borrow;
import org.lq.view.retun.V_Return;

/**
 * 读者主界面按钮事件监听
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.view.reader
 * @date 2020年9月4日下午6:27:25
 */
public class BtnActionListener implements ActionListener {

	private JTabbedPane tab;
	private int id;
	public BtnActionListener(JTabbedPane tab,int id) {
		this.tab = tab;
		this.id = id;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		switch (btn.getText()) {
		case "查询图书":
			tab.removeAll();
			tab.addTab("查询图书", new V_booksUser().getContentPane());
			tab.updateUI();
			break;
		case "借阅图书":
			tab.removeAll();
			tab.addTab("借阅图书", new V_Borrow(id).getContentPane());
			tab.updateUI();
			break;
		case "归还图书":
			tab.removeAll();
			tab.addTab("归还图书", new V_Return().getContentPane());
			tab.updateUI();
			break;
		case "修改密码":
			tab.removeAll();
			tab.addTab("修改密码", new V_UpdatePassword(id).getContentPane());
			tab.updateUI();
			break;
		}
	}

}
