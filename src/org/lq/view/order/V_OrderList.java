package org.lq.view.order;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Document;

import org.lq.entity.Books;
import org.lq.entity.OrderList;
import org.lq.entity.Supplier;
import org.lq.service.BooksService;
import org.lq.service.DictionaryService;
import org.lq.service.OrderListService;
import org.lq.service.SupplierService;
import org.lq.service.impl.BooksServiceImpl;
import org.lq.service.impl.DictionaryServiceImpl;
import org.lq.service.impl.OrderListServiceImpl;
import org.lq.service.impl.SupplierServiceImpl;
import org.lq.util.CastUtil;
import org.lq.view.books.DocumentAdapter;
import org.lq.view.books.V_booksAdmin;
import org.lq.view.supplier.V_SupplierAdd;

import com.eltima.components.ui.DatePicker;
/**
 * 
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.view.order
 * @时间 2020年8月30日
 *
 */
public class V_OrderList extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField bookId_textField;
	private JTextField bookName_textField;
	private JTextField OrderListNum_textField;
	private JTextField purchasePrice_textField;
	private JTextField totalPrice_textField;
	private JTextField textField_5;
	private JTextField textField_6;
	private JComboBox supplier_comboBox;
	
	private OrderListService os = new OrderListServiceImpl();
	private SupplierService ss = new SupplierServiceImpl();
	private BooksService bs = new BooksServiceImpl();
	DictionaryService ds = new DictionaryServiceImpl();
	private JComboBox search_comboBox;
	private JTable table_2;
	private JTextField textField;
	
	private DatePicker startDP;
	private DatePicker endDP;
	private JTextField booktype_textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_OrderList frame = new V_OrderList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_OrderList() {
		setTitle("图书采购");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 919, 564);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setBounds(10, 10, 880, 505);
		splitPane.setDividerLocation(.35);
		contentPane.add(splitPane);
		
		JPanel panel = new JPanel();
		splitPane.setLeftComponent(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("[ 图书编号 ]");
		lblNewLabel.setBounds(10, 42, 72, 15);
		panel.add(lblNewLabel);
		
		bookId_textField = new JTextField();
		bookId_textField.setEditable(false);
		bookId_textField.setBounds(92, 39, 102, 21);
		panel.add(bookId_textField);
		bookId_textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("[ 图书名称 ]");
		lblNewLabel_1.setBounds(10, 79, 72, 15);
		panel.add(lblNewLabel_1);
		
		bookName_textField = new JTextField();
		bookName_textField.setEditable(false);
		bookName_textField.setBounds(92, 76, 102, 21);
		panel.add(bookName_textField);
		bookName_textField.setColumns(10);
		
		JLabel lblNewLabel_1_1 = new JLabel("[ 图书类别 ]");
		lblNewLabel_1_1.setBounds(10, 116, 72, 15);
		panel.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_2 = new JLabel("[ 入库数量 ]");
		lblNewLabel_2.setBounds(256, 42, 72, 15);
		panel.add(lblNewLabel_2);
		
		OrderListNum_textField = new JTextField();
		Document num_document = OrderListNum_textField.getDocument();
		num_document.addDocumentListener(new DocumentAdapter() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				updateTotalPrice();
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				updateTotalPrice();
			}
		});
		OrderListNum_textField.setBounds(338, 39, 102, 21);
		panel.add(OrderListNum_textField);
		OrderListNum_textField.setColumns(10);
		
		JLabel lblNewLabel_2_1 = new JLabel("[ 进    价 ]");
		lblNewLabel_2_1.setBounds(256, 79, 72, 15);
		panel.add(lblNewLabel_2_1);
		
		purchasePrice_textField = new JTextField();
		Document price_document = purchasePrice_textField.getDocument();
		price_document.addDocumentListener(new DocumentAdapter() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				updateTotalPrice();
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				updateTotalPrice();
			}
		});
		purchasePrice_textField.setColumns(10);
		purchasePrice_textField.setBounds(338, 76, 102, 21);
		panel.add(purchasePrice_textField);
		
		JLabel lblNewLabel_2_1_1 = new JLabel("[ 总 金 额 ]");
		lblNewLabel_2_1_1.setBounds(256, 116, 72, 15);
		panel.add(lblNewLabel_2_1_1);
		
		totalPrice_textField = new JTextField();
		totalPrice_textField.setText(" 0.0");
		totalPrice_textField.setEditable(false);
		totalPrice_textField.setBounds(361, 113, 79, 21);
		panel.add(totalPrice_textField);
		totalPrice_textField.setColumns(10);
		
		JLabel lblNewLabel_2_1_1_1 = new JLabel("[ 供 货 商 ]");
		lblNewLabel_2_1_1_1.setBounds(502, 42, 72, 15);
		panel.add(lblNewLabel_2_1_1_1);
		
		supplier_comboBox = new JComboBox();
		supplier_comboBox.setBounds(584, 38, 102, 23);
		panel.add(supplier_comboBox);
		
		JLabel lblNewLabel_2_1_2 = new JLabel("[ 备    注 ]");
		lblNewLabel_2_1_2.setBounds(502, 79, 72, 15);
		panel.add(lblNewLabel_2_1_2);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(584, 79, 128, 89);
		panel.add(scrollPane_1);
		
		JEditorPane editorPane = new JEditorPane();
		scrollPane_1.setViewportView(editorPane);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_booksAdmin booksAdmin = new V_booksAdmin(1);
				booksAdmin.setVisible(true);
				
				booksAdmin.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						int booksId = booksAdmin.getBooksId();
						Books book = bs.getById(booksId);
						
						bookId_textField.setText(book.getBookNumber());
						bookName_textField.setText(book.getBookName());
						booktype_textField.setText(ds.getById(book.getBookType()).getName());
						
					}
				});
			}
		});
		btnNewButton.setIcon(new ImageIcon(V_OrderList.class.getResource("/images/goto.png")));
		btnNewButton.setBounds(199, 38, 28, 23);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("保存");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderList orderList = new OrderList();
				orderList.setBookId(bookId_textField.getText());
				orderList.setBookName(bookName_textField.getText());
				orderList.setNumber(CastUtil.castInt(OrderListNum_textField.getText()));
				orderList.setPurchasePrice(CastUtil.castDouble(purchasePrice_textField.getText()));
				orderList.setTotalPrice(CastUtil.castDouble(totalPrice_textField.getText()));
				orderList.setNote(editorPane.getText());
				Date date = new Date();
				orderList.setDate(new Timestamp(date.getTime()));
				String strDate = CastUtil.format(date, CastUtil.TIME_DATE_PATERN_3);
				orderList.setOrderNo(returnInt(strDate));
				
				Supplier supplier = (Supplier) supplier_comboBox.getSelectedItem();
				orderList.setSupplier(supplier.getId());
				
				orderList.setBookType(booktype_textField.getText());
				
				Books book = bs.getBookByBookNumber(orderList.getBookId());    //根据图书的id获取到图书对象
				if (book != null) {
					if (os.save(orderList)) {
						book.setBookCount(book.getBookCount() + orderList.getNumber());   //改变现有数量
						bs.update(book);                                //修改数据库中的值
						JOptionPane.showMessageDialog(contentPane, "订单添加成功!");
						initTable();
					}else {
						JOptionPane.showMessageDialog(contentPane, "订单添加失败!");
					}
				}else {
					JOptionPane.showMessageDialog(contentPane, "添加书库不存在的新图书请到图书添加页面!");
				}
				
			}
		});
		btnNewButton_1.setIcon(new ImageIcon(V_OrderList.class.getResource("/images/save.png")));
		btnNewButton_1.setBounds(621, 7, 93, 23);
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("取消");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton_2.setIcon(new ImageIcon(V_OrderList.class.getResource("/images/reset.png")));
		btnNewButton_2.setBounds(730, 7, 93, 23);
		panel.add(btnNewButton_2);
		
		search_comboBox = new JComboBox();
		search_comboBox.setModel(new DefaultComboBoxModel(new String[] {"显示所有", "显示当年", "显示当月", "显示当日"}));
		search_comboBox.setBounds(10, 7, 93, 23);
		panel.add(search_comboBox);
		
		startDP = new DatePicker();
		startDP.setBounds(133, 8, 140, 21);
		panel.add(startDP);
		
		endDP = new DatePicker();
		endDP.setBounds(320, 8, 140, 21);
		panel.add(endDP);
		
		JButton btnNewButton_3 = new JButton("查询");
		btnNewButton_3.setIcon(new ImageIcon(V_OrderList.class.getResource("/images/查询.png")));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//下拉列表展示方法
				String value = search_comboBox.getSelectedItem().toString();
				initTable(os.getByDateName(value));
				
				//选择日期的展示方法
				//避免冲突,只有选择为显示所以才允许查询
				if ("显示所有".equals(value)) {
					String start = startDP.getInnerTextField().getText();
					String end = endDP.getInnerTextField().getText();
					//有开始时间,未选择结束时间
					if (!"".equals(start) && "".equals(end)) {
						try {
							Date startDate = CastUtil.parseUtilDate(start, CastUtil.TIME_DATE_PATTERN_1);
							Date endDate = new Date();         //默认截止到当前时间
							initTable(os.getOrderByDate(startDate, endDate));
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					}else if ("".equals(start) && !"".equals(end)) {
						//有结束时间,无开始时间
						try {
							Date startDate = new Date(0, 0, 1);  //起始时间1989年12月31日0点0分0秒加一天
							Date endDate = CastUtil.parseUtilDate(end, CastUtil.TIME_DATE_PATTERN_1);
							initTable(os.getOrderByDate(startDate, endDate));
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					}else if ("".equals(start) && "".equals(end)) {
						//开始和结束时间均为空
						initTable(os.findAll());
					}else {
						//均有值
						try {
							Date startDate = CastUtil.parseUtilDate(start, CastUtil.TIME_DATE_PATTERN_1);
							Date endDate = CastUtil.parseUtilDate(end, CastUtil.TIME_DATE_PATTERN_1);
							initTable(os.getOrderByDate(startDate, endDate));
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					}
				}else {
					JOptionPane.showMessageDialog(contentPane, "必须显示所有才可以更精确的查询哦!");
				}
				
			}
		});
		btnNewButton_3.setBounds(502, 7, 93, 23);
		panel.add(btnNewButton_3);
		
		JButton btnNewButton_4_1 = new JButton("");
		btnNewButton_4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_SupplierAdd add = new V_SupplierAdd();
				add.setBounds(V_OrderList.this.getX(), V_OrderList.this.getY(), add.getWidth(), add.getHeight());
				add.setVisible(true);
				
				add.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						initCombox();
					}
				});
			}
		});
		btnNewButton_4_1.setIcon(new ImageIcon(V_OrderList.class.getResource("/images/add.png")));
		btnNewButton_4_1.setBounds(696, 38, 28, 23);
		panel.add(btnNewButton_4_1);
		
		textField = new JTextField();
		textField.setText(" ¥");
		textField.setEditable(false);
		textField.setBounds(338, 113, 28, 21);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton startDP_reset = new JButton("");
		startDP_reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startDP.getInnerTextField().setText("");
			}
		});
		startDP_reset.setIcon(new ImageIcon(V_OrderList.class.getResource("/images/reset.png")));
		startDP_reset.setBounds(283, 7, 23, 23);
		panel.add(startDP_reset);
		
		JButton endDP_reset = new JButton("");
		endDP_reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				endDP.getInnerTextField().setText("");
			}
		});
		endDP_reset.setIcon(new ImageIcon(V_OrderList.class.getResource("/images/reset.png")));
		endDP_reset.setBounds(470, 7, 23, 23);
		panel.add(endDP_reset);
		
		booktype_textField = new JTextField();
		booktype_textField.setEditable(false);
		booktype_textField.setColumns(10);
		booktype_textField.setBounds(92, 113, 102, 21);
		panel.add(booktype_textField);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		table_2 = new JTable();
		table_2.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"\u5355\u53F7", "\u56FE\u4E66\u7F16\u53F7", "\u56FE\u4E66\u540D\u79F0", "\u5165\u5E93\u6570\u91CF", "\u5355\u4EF7", "\u603B\u91D1\u989D", "\u4F9B\u8D27\u5546", "\u65E5\u671F"
			}
		));
		table_2.getColumnModel().getColumn(0).setPreferredWidth(150);
		table_2.getColumnModel().getColumn(7).setPreferredWidth(150);
		scrollPane.setViewportView(table_2);
		
		initTable();
		initCombox();
	}
	/**
	 * 生成4位随机整数,加到参数末尾
	 * @param str
	 * @return
	 */
	public String returnInt(String str) {
		Random r = new Random();
		for (int i = 0; i < 4; i++) {
			str += r.nextInt(10);
		}
		return str;
	}
	/**
	 * 初始化表格
	 */
	public void initTable() {
		initTable(os.findAll());
	}
	/**
	 * 初始化表格
	 * @param list
	 */
	public void initTable(List<OrderList> list) {
		DefaultTableModel dtm = (DefaultTableModel) table_2.getModel();
		
		int rowCount = dtm.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			dtm.removeRow(0);
		}
		for (OrderList orderList : list) {
			dtm.addRow(new Object[] {
				orderList.getOrderNo(),
				orderList.getBookId(),
				orderList.getBookName(),
				orderList.getNumber(),
				orderList.getPurchasePrice(),
				orderList.getTotalPrice(),
				ss.getSupplierById(orderList.getSupplier()).getName(),
				orderList.getDate()
			});
		}
		
	}
	/**
	 * 初始化下拉列表
	 */
	public void initCombox() {
		
		List<Supplier> supplierList = ss.findSupplierAll();
		Supplier supplier = new Supplier();
		supplier.setId(-1);
		supplier.setName("--请选择--");
		
		supplierList.add(0,supplier);
		Supplier[] supplier_array = supplierList.toArray(new Supplier[] {});
		DefaultComboBoxModel<Supplier> supplierDBM = new DefaultComboBoxModel<Supplier>(supplier_array);
		supplier_comboBox.setModel(supplierDBM);
	}
	/**
	 * 更新总金额
	 */
	public void updateTotalPrice() {
		double price = CastUtil.castDouble(purchasePrice_textField.getText());
		int num = CastUtil.castInt(OrderListNum_textField.getText());
		double totalPrice = price * num;
		totalPrice_textField.setText(totalPrice+"");
	}
}


