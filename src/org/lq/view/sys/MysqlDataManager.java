package org.lq.view.sys;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.lq.util.CastUtil;

public class MysqlDataManager extends JFrame {

	private JPanel contentPane;
	public static JCheckBox cb_auto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MysqlDataManager frame = new MysqlDataManager();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MysqlDataManager() {
		setResizable(false);
		setTitle("数据库管理");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 408, 426);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("自动备份：");
		lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 16));
		lblNewLabel.setBounds(95, 62, 91, 19);
		contentPane.add(lblNewLabel);

		cb_auto = new JCheckBox("开启");
		cb_auto.setFont(new Font("宋体", Font.PLAIN, 16));

		cb_auto.setBounds(218, 54, 120, 35);
		contentPane.add(cb_auto);

		JButton btnNewButton = new JButton("备份数据库");
		btnNewButton.setIcon(new ImageIcon(MysqlDataManager.class.getResource("/images/备份_复制.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser save = new JFileChooser();
				String dir = System.getProperty("user.dir");
				save.setSelectedFile(new File(dir + "/data/librarymanager-"
						+ CastUtil.format(new Date(), CastUtil.TIME_DATE_PATERN_2) + ".sql"));
				int showSaveDialog = save.showSaveDialog(save);//0代表返回确认，1代表返回取消
				if (showSaveDialog==0) {
					File file = save.getSelectedFile();
					try {
						Runtime.getRuntime().exec("cmd.exe /c mysqldump -uroot -proot --databases librarymanager > "
								+ file.getAbsolutePath());
						JOptionPane.showMessageDialog(MysqlDataManager.this, "保存成功！！！");
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				
				
			}
		});
		btnNewButton.setFont(new Font("宋体", Font.PLAIN, 18));
		btnNewButton.setBounds(95, 122, 190, 53);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("恢复数据库");
		btnNewButton_1.setIcon(new ImageIcon(MysqlDataManager.class.getResource("/images/恢复备份.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JFileChooser open = new JFileChooser();
				String dir = System.getProperty("user.dir");
				open.setSelectedFile(new File(dir + "/data/"));
				open.showOpenDialog(null);
				File file = open.getSelectedFile();
				open.setFileFilter(new FileNameExtensionFilter("SQL fiels", "sql"));

				if (file!=null) {
					try {
						Runtime.getRuntime().exec("cmd.exe /c mysql -uroot -proot < " + file.getAbsolutePath());
						JOptionPane.showMessageDialog(MysqlDataManager.this, "恢复成功！！！");
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}else {
					JOptionPane.showMessageDialog(MysqlDataManager.this, "未选择恢复文件！");
				}
			}
		});
		btnNewButton_1.setFont(new Font("宋体", Font.PLAIN, 18));
		btnNewButton_1.setBounds(95, 221, 190, 53);
		contentPane.add(btnNewButton_1);

		JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setText("备注：开启自动备份后，每天22:00将进行自动数据库备份");
		textPane.setBounds(149, 326, 222, 51);
		contentPane.add(textPane);
	}
}
