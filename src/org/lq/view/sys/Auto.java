package org.lq.view.sys;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;

import org.lq.entity.Sql;
import org.lq.service.SqlService;
import org.lq.service.impl.SqlServiceImpl;
import org.lq.util.CastUtil;
import org.lq.view.enroll.V_AdminManagerMain;

/**
 * 到达指定时间进行自动数据库备份
 * 
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.view.sys
 * @date 2020年8月30日下午12:08:59
 */
public class Auto implements Runnable {
	boolean flag ;
	@Override
	public void run() {
		
		SqlService ss = new SqlServiceImpl();
		while (true) {
			// 获取到备份状态
			try {
				Thread.currentThread();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Sql s = ss.getById(1);
			// 获得备份窗口
			MysqlDataManager data = V_AdminManagerMain.getData();
			if (s.getSatatus() == 1) {// 获取到结果是开启备份
				flag =true;
				data.cb_auto.setSelected(true);// 勾选
				// 添加勾选的事件监听
				data.cb_auto.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (!data.cb_auto.isSelected()) {// 如果取消了勾选，更改数据库状态
							s.setSatatus(0);
							ss.update(s);
							flag =false;
//							System.out.println("1");
						}
					}
				});
			} else {//获取到结果未开启自动备份
				flag =false;
				data.cb_auto.setSelected(false);// 取消勾选
				//添加事件监听
				data.cb_auto.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(data.cb_auto.isSelected()){
							data.cb_auto.setSelected(true);//此行代码执行较慢...
							s.setSatatus(1);
							ss.update(s);
							flag = true;
//							System.out.println("4");
						}
					}
				});
			}
			backups(flag);
		}
	}

	public void backups(boolean flag) {
		if (flag) {

//			try {
//				Thread.currentThread();
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			System.out.println("备份后台监控中...");
			// 每隔一秒获取一次当前时间，转为字符串，截取最后六位，与220000进行比较，比较成功进行备份操作
			String format = CastUtil.format(new Date(), CastUtil.TIME_DATE_PATERN_4);
			String s = format.substring(8);
			if (s.equals("220000")) {
				String dir = System.getProperty("user.dir");
				File file = new File(dir + "/data/librarymanager-"
						+ CastUtil.format(new Date(), CastUtil.TIME_DATE_PATERN_2) + ".sql");
				try {
					Runtime.getRuntime().exec("cmd.exe /c mysqldump -uroot -proot --databases librarymanager > "
							+ file.getAbsolutePath());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

}
