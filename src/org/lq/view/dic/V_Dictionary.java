package org.lq.view.dic;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

import org.lq.entity.Dictionary;
import org.lq.entity.TypeID;
import org.lq.service.BooksService;
import org.lq.service.DictionaryService;
import org.lq.service.impl.BooksServiceImpl;
import org.lq.service.impl.DictionaryServiceImpl;
import org.lq.util.CastUtil;

import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;

public class V_Dictionary extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private DictionaryService ds = new DictionaryServiceImpl();
	private BooksService bs = new BooksServiceImpl();
	private JTree tree_type;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_Dictionary frame = new V_Dictionary();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_Dictionary() {
		setTitle("字典表管理");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 381, 468);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel type = new JPanel();
		tabbedPane.addTab("图书类别", null, type, "图书类别设置");
		type.setLayout(new BorderLayout(0, 0));

		tree_type = new JTree();
		type.add(tree_type, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);



		JButton btn_new = new JButton("添加类别");
		btn_new.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int tableIndex = tabbedPane.getSelectedIndex();//根据选项卡获取选择的下标
				String title = "";//设置选项卡的标题
				Dictionary dic = new Dictionary();
				switch (tableIndex) {
				case 0:
					title = "图书类别";
					dic.setTypeid(TypeID.图书类别);
					break;
				}
				String value = JOptionPane.showInputDialog(V_Dictionary.this, "请输入"+title+"的名称",title,JOptionPane.PLAIN_MESSAGE);
				boolean bool = ds.getByName(value);//根据名称查询
				if(bool) {
					//如果为true,则要添加的数据已经存在,提示用户
					JOptionPane.showMessageDialog(V_Dictionary.this, "已经存在,不可重复添加");
				}else {
					//如果为false,则判断添加
					if(value !=null && !"".equals(value)) {
						dic.setName(value);
						showMessage(ds.save(dic), "添加");
					}
				}
			}
		});
		panel.add(btn_new);

		JButton btn_reset = new JButton("修改类别");
		btn_reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTree tree = null;//动态的获取不同的树
				String title = "";//设置选项卡的标题
				int tabIndex = tabbedPane.getSelectedIndex();//根据选项卡获取选择的下标
				switch (tabIndex) {
				case 0:
					title = "图书类别";
					tree = tree_type;
					break;
				}
				if(tree.getSelectionPath() != null) {
					//tree.getSelectionPath().getLastPathComponent() 获取鼠标点击的值
					String selectValue = CastUtil.castString(tree.getSelectionPath().getLastPathComponent());//将获取到的值装换为字符串类型
					Object value =JOptionPane.showInputDialog(V_Dictionary.this, "请输入"+title+"的名称:",title,JOptionPane.PLAIN_MESSAGE,null,null,selectValue);
					boolean bool = ds.getByName(CastUtil.castString(value));//根据名称查询
					if(bool) {
						//如果为true,则要添加的数据已经存在,提示用户
						JOptionPane.showMessageDialog(V_Dictionary.this, "已经存在该值,不可修改为此值");
					}
					else {
						if(value !=null && ! "".equals(value)) {
							//获取选中的节点
							DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent();
							Dictionary d= (Dictionary) dmtn.getUserObject();
							d.setName(CastUtil.castString(value));
							showMessage(ds.update(d), "修改");;
						}
					}
				}else {
					showMessage(false, "请选择修改的节点,");
				}
			}
		});
		panel.add(btn_reset);

		JButton btn_update = new JButton("删除类别");
		btn_update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTree tree = null;//动态的获取不同的树
				int tabIndex = tabbedPane.getSelectedIndex();//根据选项卡获取选择的下标
				switch (tabIndex) {
				case 0:
					tree = tree_type;
					break;
				}
				//获取树节点的数据模型
				DefaultTreeModel dtm = (DefaultTreeModel) tree.getModel();
				TreePath selectionPath = tree.getSelectionPath();//通过节点的数据模型,删除选中的节点对象
				if(selectionPath != null) {
					//获取删除节点对象的最后一节点
					MutableTreeNode mtn = (MutableTreeNode) selectionPath.getLastPathComponent();//获取鼠标点击的值
					//询问是否确定删除
					int n = JOptionPane.showConfirmDialog(V_Dictionary.this, "你确定删除吗?","删除",JOptionPane.OK_CANCEL_OPTION);
					if(n == 0) {
						//删除根据编号删除
						DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) mtn;
						//获取添加到节点中的对象,节点存储的是什么,获取的就是什么
						Dictionary dict = (Dictionary) dmtn.getUserObject();//被选中的字典对象
						boolean bool = bs.getBybookType(dict.getId());//根据BooksServiceImpl的方法返回值
						if(bool) {
							JOptionPane.showMessageDialog(V_Dictionary.this, "该内容有值,不能删除");
						}else {
							showMessage(ds.delete(dict.getId()), "删除");
							dtm.removeNodeFromParent(mtn);//通过节点的数据模型,删除选中的节点对象
						}
					}
				}else {
					//未选中节点
					JOptionPane.showMessageDialog(V_Dictionary.this, "请选择节点后删除!");
				}
			}
		});
		panel.add(btn_update);
		initTree();//初始化树
	}


	/**
	 * 消息提示
	 * @param bool
	 * @param msg
	 */
	public void showMessage(boolean bool,String msg) {
		if(bool) {
			JOptionPane.showMessageDialog(V_Dictionary.this, msg+"成功!",msg,JOptionPane.INFORMATION_MESSAGE);
			initTree();//成功后,调用初始化树
		}else {
			JOptionPane.showMessageDialog(V_Dictionary.this, msg+"失败!",msg,JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * 初始化所有树
	 */
	public void initTree() {
		initTypeTree();
	}
	/**
	 * 初始化图书类型的树节点的方法
	 */
	public void initTypeTree() {
		List<Dictionary> typeList = ds.findByTypeid(TypeID.图书类别);
		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("图书类别节点");
		for (Dictionary type : typeList) {
			dmtn.add(new DefaultMutableTreeNode(type));
		}
		DefaultTreeModel dtm = new DefaultTreeModel(dmtn);
		tree_type.setModel(dtm);
	}

}
