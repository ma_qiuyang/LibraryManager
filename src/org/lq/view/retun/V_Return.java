package org.lq.view.retun;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.lq.entity.Books;
import org.lq.entity.Borrow;
import org.lq.entity.Reader;
import org.lq.service.BooksService;
import org.lq.service.BorrowService;
import org.lq.service.ReaderService;
import org.lq.service.impl.BooksServiceImpl;
import org.lq.service.impl.BorrowServiceImpl;
import org.lq.service.impl.ReaderServiceImpl;
import org.lq.util.CastUtil;
import org.lq.view.enroll.V_Enroll;
import org.lq.view.reader.V_Reader;

import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import java.awt.FlowLayout;

public class V_Return extends JFrame {

	private JPanel contentPane;
	private static JTable table;
	private JMenuItem returnbooks;
	private BorrowService bs = new BorrowServiceImpl();
	private BooksService books = new BooksServiceImpl();
	private ReaderService rs = new ReaderServiceImpl();
	private JPanel panel;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private int id = new V_Enroll().getId();
		
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_Return frame = new V_Return();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_Return() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 524, 594);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"\u7F16\u53F7", "\u56FE\u4E66\u7F16\u53F7", "\u8BFB\u8005\u7F16\u53F7", "\u56FE\u4E66\u540D\u79F0", "\u8BFB\u8005\u540D\u79F0", "\u56FE\u4E66\u7C7B\u522B", "\u5F52\u8FD8\u72B6\u6001"
			}
		));
		scrollPane.setViewportView(table);
		
		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(table, popupMenu);
		
		returnbooks = new JMenuItem("归还图书");
		returnbooks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//归还图书
				int index = table.getSelectedRow();//获取选中的行
				int id = CastUtil.castInt(table.getValueAt(index, 0));//获取选中行的第一列
				/**
				 * 修改借书表
				 */
				Borrow borr = bs.getById(id);
				if(borr.getStatus() == 1) {
					JOptionPane.showMessageDialog(V_Return.this, "该图书已归还","报错",JOptionPane.ERROR_MESSAGE);
				}else {
					borr.setStatus(1);//把borrow里面的状态改为已还
					bs.update(borr);//把修改过的borrow重新保存到borrow表里面
					/**
					 * 修改图书表
					 */
					Books booksbyId = books.getBookByBookNumber(borr.getBookId());
					int count = booksbyId.getBookCount()+1;
					if(booksbyId.getBookCount() == 1){
						booksbyId.setStatus(1);//如果该书之前的现存数量为1,则把该书的状态改为可借
					}
					booksbyId.setBookCount(count);//把修改过后的值重新修改赋值
					books.update(booksbyId);//再把该书修改过后保存在数据库中
					/**
					 * 修改读者表
					 */
					int personid = CastUtil.castInt(borr.getPersonId());
					Reader personbyId = rs.getById(personid);
					int notreturnnum = personbyId.getNotreturnnum() - 1;//把未还数量减1
					personbyId.setNotreturnnum(notreturnnum);//重新赋值
					rs.update(personbyId);//赋值之后重新保存
					JOptionPane.showMessageDialog(V_Return.this, "还书成功!","还书",JOptionPane.INFORMATION_MESSAGE);
					List<Borrow> byPersonId = bs.getByPersonId(personid);
					initTable(byPersonId);
				}
			}
		});
		popupMenu.add(returnbooks);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnNewButton = new JButton("显示未还");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//显示未还的数量
				List<Borrow> byStatus = bs.getByStatus(id,0);//这里面的传入id也需要改
				initTable(byStatus);
			}
		});
		panel.add(btnNewButton);
		
		btnNewButton_1 = new JButton("显示已还");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//显示已还的数量
				List<Borrow> byStatus = bs.getByStatus(id,1);//这里面的传入id也需要改
				initTable(byStatus);
			}
		});
		panel.add(btnNewButton_1);
		//初始化表格
		List<Borrow> byPersonId = bs.getByPersonId(id);//这里需要修改,需要把0换成从读者主界面获取到的id
		initTable(byPersonId);
	}
	
	/**
	  * 初始化表格
	 * @param list
	 */
	public void initTable(List<Borrow> list) {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int count = dtm.getRowCount();//获取表中的总行数
		for (int i = 0; i < count; i++) {
			dtm.removeRow(0);//清除表中的所有元素
		}
		for (Borrow borrow : list) {
			dtm.addRow(new Object[] {
				borrow.getId(),
				borrow.getBookId(),
				borrow.getPersonId(),
				borrow.getBookName(),
				borrow.getPersonName(),
				borrow.getBookType(),
				borrow.getStatus() == 0 ? "未还" : "已还"
			});
		}
	}
	/**
	 * 初始化表格
	 */
	public void initTable() {
		initTable(bs.findAll());
	}
	
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				int index = table.rowAtPoint(e.getPoint());//获取点击的行的座标
				table.setRowSelectionInterval(index, index);//选中点击的行
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
