package org.lq.view.borrow;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.lq.entity.Books;
import org.lq.entity.Borrow;
import org.lq.entity.Reader;
import org.lq.service.BooksService;
import org.lq.service.BorrowService;
import org.lq.service.DictionaryService;
import org.lq.service.ReaderService;
import org.lq.service.impl.BooksServiceImpl;
import org.lq.service.impl.BorrowServiceImpl;
import org.lq.service.impl.DictionaryServiceImpl;
import org.lq.service.impl.ReaderServiceImpl;
import org.lq.util.CastUtil;
import org.lq.view.enroll.V_Enroll;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.awt.Color;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.JTable;
import java.awt.FlowLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPopupMenu;
import java.awt.Component;
import javax.swing.JMenuItem;
import javax.swing.border.EtchedBorder;
import javax.swing.JEditorPane;
/**
 * 图书借阅窗口
 * @author 李冠良
 * @phone 18336095308
 *
 * @package org.lq.view.borrow
 * @时间 2020年8月24日
 *
 */
public class V_Borrow extends JFrame {

	private JPanel contentPane;
	private JTextField readerid_textField;
	private JTextField textField_id;
	private JTextField textField_name;
	private JTextField textField_gender;
	private JTextField textField_type;
	private JTextField textField_limit;
	private JTextField textField_numlimit;
	private JTextField textField_notReturnNum;
	private JTextField textField_registerTime;
	private static JTable table;
	private JTextField textField_book;

	private ReaderService rs = new ReaderServiceImpl();
	private Reader reader = null;
	private BooksService bs = new BooksServiceImpl();
	private JEditorPane editorPane;
	
	private BorrowService borrowService = new BorrowServiceImpl();
	private DictionaryService ds = new DictionaryServiceImpl();
	private static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_Borrow frame = new V_Borrow(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_Borrow(int id) {
		setResizable(false);
		setTitle("图书借阅");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 893, 745);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(
				new TitledBorder(null, "\u8BFB\u8005\u4FE1\u606F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 10, 859, 153);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("[ 编    号 ]");
		lblNewLabel_1.setBounds(33, 41, 74, 15);
		panel.add(lblNewLabel_1);

		textField_id = new JTextField();
		textField_id.setEditable(false);
		textField_id.setBounds(109, 38, 99, 21);
		panel.add(textField_id);
		textField_id.setColumns(10);

		textField_name = new JTextField();
		textField_name.setEditable(false);
		textField_name.setBounds(311, 38, 99, 21);
		panel.add(textField_name);
		textField_name.setColumns(10);

		textField_gender = new JTextField();
		textField_gender.setEditable(false);
		textField_gender.setColumns(10);
		textField_gender.setBounds(517, 38, 99, 21);
		panel.add(textField_gender);

		textField_type = new JTextField();
		textField_type.setEditable(false);
		textField_type.setColumns(10);
		textField_type.setBounds(726, 38, 99, 21);
		panel.add(textField_type);

		JLabel lblNewLabel_1_1 = new JLabel("[ 姓    名 ]");
		lblNewLabel_1_1.setBounds(235, 41, 74, 15);
		panel.add(lblNewLabel_1_1);

		JLabel lblNewLabel_1_6 = new JLabel("[ 性    别 ]");
		lblNewLabel_1_6.setBounds(442, 41, 74, 15);
		panel.add(lblNewLabel_1_6);

		JLabel lblNewLabel_1_7 = new JLabel("[ 类    别 ]");
		lblNewLabel_1_7.setBounds(649, 41, 74, 15);
		panel.add(lblNewLabel_1_7);

		JLabel lblNewLabel_1_2 = new JLabel("[ 借书期限 ]");
		lblNewLabel_1_2.setBounds(33, 90, 74, 15);
		panel.add(lblNewLabel_1_2);

		JLabel lblNewLabel_1_2_1 = new JLabel("[ 数量限制 ]");
		lblNewLabel_1_2_1.setBounds(235, 90, 74, 15);
		panel.add(lblNewLabel_1_2_1);

		JLabel lblNewLabel_1_2_2 = new JLabel("[ 未还书数 ]");
		lblNewLabel_1_2_2.setBounds(442, 90, 74, 15);
		panel.add(lblNewLabel_1_2_2);

		textField_limit = new JTextField();
		textField_limit.setEditable(false);
		textField_limit.setColumns(10);
		textField_limit.setBounds(109, 87, 99, 21);
		panel.add(textField_limit);

		textField_numlimit = new JTextField();
		textField_numlimit.setEditable(false);
		textField_numlimit.setColumns(10);
		textField_numlimit.setBounds(311, 87, 99, 21);
		panel.add(textField_numlimit);

		textField_notReturnNum = new JTextField();
		textField_notReturnNum.setEditable(false);
		textField_notReturnNum.setColumns(10);
		textField_notReturnNum.setBounds(517, 87, 99, 21);
		panel.add(textField_notReturnNum);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setBounds(10, 183, 859, 523);
		splitPane.setDividerLocation(0.25); // 设置左右比例(左边占多少)
		contentPane.add(splitPane);

		JPanel panel_2 = new JPanel();
		splitPane.setRightComponent(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));

		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_3.getLayout();
		flowLayout.setHgap(30);
		flowLayout.setVgap(12);
		panel_2.add(panel_3, BorderLayout.NORTH);

		JLabel lblNewLabel_2 = new JLabel("图书编号/图书名称:");
		panel_3.add(lblNewLabel_2);

		textField_book = new JTextField();
		panel_3.add(textField_book);
		textField_book.setColumns(10);

		JButton btnNewButton_1 = new JButton("查找");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Books> list = bs.query(textField_book.getText());
				initTable(list);
			}
		});
		btnNewButton_1.setIcon(new ImageIcon(V_Borrow.class.getResource("/images/search.png")));
		panel_3.add(btnNewButton_1);

		JScrollPane scrollPane = new JScrollPane();
		panel_2.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null, null }, },
				new String[] { "\u56FE\u4E66\u7F16\u53F7", "\u56FE\u4E66\u540D\u79F0", "\u56FE\u4E66\u7C7B\u578B",
						"\u4F5C\u8005", "\u72B6\u6001" }));
		scrollPane.setViewportView(table);

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(table, popupMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("借阅选定图书");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//用户输入信息正确之后reader不会为空
				if (reader != null) {
					//判断用户是否还能借阅图书
					if (reader.getNotreturnnum() < reader.getNum()) {
						int index = table.getSelectedRow();
						String bookNumber = CastUtil.castString(table.getValueAt(index, 0));
						Books book = bs.getBookByBookNumber(bookNumber);
						//判断图书状态
						if (book.getStatus() == 1) {
							Borrow borrow = new Borrow();
							borrow.setBookId(book.getBookNumber());
							borrow.setPersonId(reader.getId());
							borrow.setBookName(book.getBookName());
							borrow.setPersonName(reader.getName());
							borrow.setBookType(ds.getById(book.getBookType()).getName());
							//获取当前时间作为借书时间
							Date date = new Date();
							long currentTime = date.getTime();
							borrow.setBorrowTime(new Timestamp(currentTime));
							
							int days = reader.getBorrowTime();   //这个borrowTime是reader里面的允许的借书天数
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(date);
							calendar.add(calendar.DATE, days);
							
							//应还时间为当前借书时间加上借阅者的允许借书天数
							borrow.setShouldReturnTime(new Timestamp(calendar.getTime().getTime()));
							
							if (borrowService.save(borrow)) {
								reader.setNotreturnnum(reader.getNotreturnnum()+1);   //未还图书数量+1
								rs.update(reader);   //修改用户的未还图书数量
								textField_notReturnNum.setText(reader.getNotreturnnum()+"");
								
								book.setBookCount(book.getBookCount()-1);  //修改图书现存数量
								//修改完获取现存数量,如果数量不大于1了,修改状态,不允许再进行借阅
								if (!(book.getBookCount() > 1)) {
									book.setStatus(0);
								}
								bs.update(book);
								
								JOptionPane.showMessageDialog(contentPane, "图书借阅成功");
								initTable(bs.findAll());
							}else {
								JOptionPane.showMessageDialog(contentPane, "图书借阅失败");
							}
							
						}else {
							JOptionPane.showMessageDialog(contentPane, "该图书不可借");
						}
					}else {
						JOptionPane.showMessageDialog(contentPane, "您的借阅数量已达上限");
					}
				}else {
					JOptionPane.showMessageDialog(contentPane, "正确输入个人信息才能借阅哦");
				}
			}
		});
		popupMenu.add(mntmNewMenuItem);

		JMenuItem mntmNewMenuItem_1 = new JMenuItem("查看图书详细信息");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				editorPane.setContentType("text/html");
				editorPane.setText(getBooksByIdReturnHtml(CastUtil.castString(table.getValueAt(index, 0))));
			}
		});
		popupMenu.add(mntmNewMenuItem_1);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		splitPane.setLeftComponent(scrollPane_1);
		
		editorPane = new JEditorPane();
		scrollPane_1.setViewportView(editorPane);
		ImageIcon icon = new ImageIcon(V_Borrow.class.getResource("/images/book.png"));
		icon.setImage(icon.getImage().getScaledInstance(150, 135, 1));

		initTable(bs.findAll());
		initReader(new V_Enroll().getId());
	}
	

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX() + 10, e.getY() + 10);

				int index = table.rowAtPoint(e.getPoint());
				table.setRowSelectionInterval(index, index);
			}
		});
	}


	/**
	 * 初始化表格
	 * 
	 * @param list
	 */
	public void initTable(List<Books> list) {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();

		int rowCount = dtm.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			dtm.removeRow(0);
		}

		for (Books books : list) {
			String status = books.getStatus() == 0 ? "不可借" : "可借";
			dtm.addRow(new Object[] { books.getBookNumber(), books.getBookName(), ds.getById(books.getBookType()).getName(),
					books.getAuthor(), status });
		}
	}
	
	/**
	 * 根据图书id获取对象,拼接HTML代码
	 * @param id
	 * @return
	 */
	public String getBooksByIdReturnHtml(String bookNumber) {
		String dir = System.getProperty("user.dir");
		StringBuilder text = new StringBuilder();
		//读取值
		try (BufferedReader br = new BufferedReader(new FileReader(dir + "/resources/books.html"));) {
			String line = "";
			while ((line = br.readLine()) != null) {
				text.append(line);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Books book = bs.getBookByBookNumber(bookNumber);

		String content = text.toString();
		content = content.replace("${bookNumber}", book.getBookNumber());
		content = content.replace("${bookName}", book.getBookName());
		content = content.replace("${PinYin}", book.getPinYinCode());
		content = content.replace("${bookCount}", book.getBookCount()+"");
		String status = book.getStatus() == 0 ? "不可借" : "可借";
		content = content.replace("${status}", status);
		content = content.replace("${author}", book.getAuthor());
		content = content.replace("${bookType}", ds.getById(book.getBookType()).getName());
		content = content.replace("${description}", book.getDescription());
		content = content.replace("${location}", book.getLocation());
		
		return content;
	}
	
	public void initReader(int id) {
		reader = rs.getById(id);
		textField_id.setText(CastUtil.castString(reader.getId()));
		textField_name.setText(reader.getName());
		textField_gender.setText(reader.getGender());
		textField_type.setText(reader.getType());
		textField_limit.setText(CastUtil.castString(reader.getBorrowTime()));
		textField_numlimit.setText(String.valueOf(reader.getNum()));
		textField_notReturnNum.setText(String.valueOf(reader.getNotreturnnum()));
	}
}
