package org.lq.view.admin;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.lq.entity.Admin;
import org.lq.service.AdminServiceDao;
import org.lq.service.impl.AdminServiceDaoImpl;
import org.lq.util.CastUtil;

public class V_admin extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField text_userName;
	private JTextField text_password;
	private JTextField text_password2;
	private JTextField text_id;
	private JButton btn_save;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_admin frame = new V_admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_admin() {
		setTitle("管理员设置");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(550, 250, 667, 413);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(328, 10, 313, 354);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"\u5E8F\u53F7", "\u7528\u6237\u540D", "\u5BC6\u7801"
			}
		));
		scrollPane.setViewportView(table);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "\u6570\u636E\u64CD\u4F5C", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 10, 313, 354);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("用户名：");
		lblNewLabel.setBounds(10, 75, 67, 27);
		panel.add(lblNewLabel);
		
		text_userName = new JTextField();
		text_userName.setBounds(87, 75, 179, 27);
		panel.add(text_userName);
		text_userName.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("登录密码：");
		lblNewLabel_1.setBounds(10, 112, 67, 27);
		panel.add(lblNewLabel_1);
		
		text_password = new JTextField();
		text_password.setBounds(87, 112, 179, 27);
		panel.add(text_password);
		text_password.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("确认密码：");
		lblNewLabel_2.setBounds(10, 149, 67, 27);
		panel.add(lblNewLabel_2);
		
		text_password2 = new JTextField();
		text_password2.setBounds(87, 149, 179, 27);
		panel.add(text_password2);
		text_password2.setColumns(10);
		
		btn_save = new JButton("添加用户");
		btn_save.setIcon(new ImageIcon(V_admin.class.getResource("/images/添加  加好友  添加朋友.png")));
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Admin s = new Admin();
				if(text_userName.getText().equals("") || text_password.getText().equals("") || text_password2.getText().equals("")) {
					JOptionPane.showMessageDialog(V_admin.this,"用户名或密码不能为空!");
				}else {
					s.setUserName(text_userName.getText());
					s.setPassword(text_password.getText());
					AdminServiceDao service = new AdminServiceDaoImpl();
					if (text_password.getText().equals(text_password2.getText())) {
						if(service.save(s)) {
							JOptionPane.showMessageDialog(V_admin.this,"添加成功!");
							initTable();
						}else {
							JOptionPane.showMessageDialog(V_admin.this, "添加失败!");
						}
					} else {
						JOptionPane.showMessageDialog(V_admin.this, "请输入一样的密码!");  
					}
				}
					
			}
		});
		btn_save.setBounds(92, 219, 107, 23);
		panel.add(btn_save);
		
		
		JButton btnNewButton_1 = new JButton("删除用户");
		btnNewButton_1.setIcon(new ImageIcon(V_admin.class.getResource("/images/删 除 .png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
				int index = table.getSelectedRow();
				if(index>=0) {
					int n =JOptionPane.showConfirmDialog(V_admin.this, "确定删除"+table.getValueAt(index, 1)+"管理员吗?","删除",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					if(n==0) {
						int id = CastUtil.castInt(table.getValueAt(index, 0));
						if(new AdminServiceDaoImpl().delete(id)) {
							JOptionPane.showMessageDialog(V_admin.this, "删除管理员成功!");
							initTable();
						}else {
							JOptionPane.showMessageDialog(V_admin.this, "删除管理员失败!");
						}
					}
				}else {
					JOptionPane.showMessageDialog(V_admin.this, "请选择管理员!");
				}
			}
		});
		btnNewButton_1.setBounds(92, 266, 107, 23);
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("修改密码");
		btnNewButton_2.setIcon(new ImageIcon(V_admin.class.getResource("/images/修改.png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				if(index>=0) {
					int id = CastUtil.castInt(table.getValueAt(index, 0));
					V_adminUpdate add = new V_adminUpdate(id,true);
					add.setBounds(V_admin.this.getX()+50, V_admin.this.getY()+20, add.getWidth(), add.getHeight());
					add.setVisible(true);
					add.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosed(WindowEvent e) {
							initTable();
						}
					});
				}else {
					JOptionPane.showMessageDialog(V_admin.this, "请选择修改的管理员!");
				}
			}
		});
		btnNewButton_2.setBounds(92, 307, 107, 27);
		panel.add(btnNewButton_2);
		
		JLabel lblNewLabel_3 = new JLabel("序号：");
		lblNewLabel_3.setBounds(10, 34, 67, 31);
		panel.add(lblNewLabel_3);
		
		text_id = new JTextField();
		text_id.setEditable(false);
		text_id.setBounds(87, 39, 179, 26);
		panel.add(text_id);
		text_id.setColumns(10);
		
		initTable();
	}
	
	/**
	 * 初始化数据
	 * @param list
	 */
	public void initTable(List<Admin> list) {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int count = dtm.getRowCount();
		for(int i = 0;i< count;i++) {
			dtm.removeRow(0);
		}
		
		for (Admin a : list) {
			dtm.addRow(new Object[] {
					a.getId(),
					a.getUserName(),
					a.getPassword()
			});
		}
	}
	

	public void initTable() {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int count = dtm.getRowCount();
		for(int i = 0;i< count;i++) {
			dtm.removeRow(0);
		}

		AdminServiceDao service = new AdminServiceDaoImpl();
		List<Admin> list = service.findAll();
		for (Admin a : list) {
			String x = a.getPassword().replaceAll(".", "*");//.代表任意字符，将添加的任意字符替换成*
			dtm.addRow(new Object[] {
					a.getId(),
					a.getUserName(),
					x
			});
		}
	}
	

}
