package org.lq.view.admin;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import org.lq.entity.Admin;
import org.lq.service.AdminServiceDao;
import org.lq.service.impl.AdminServiceDaoImpl;
import org.lq.util.CastUtil;

public class V_adminUpdate extends JFrame {

	private JPanel contentPane;
	private JTextField text_id;
	private JTextField text_userName;
	private JTextField text_password;
	private JTextField text_password2;
	private JButton btn_save;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_adminUpdate frame = new V_adminUpdate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * 修改，将管理员界面的文本框展示内容填到修改界面
	 */
	public  V_adminUpdate(int id,boolean bool) {
		this();
		Admin s = new AdminServiceDaoImpl().getById(id);
		text_id.setText(CastUtil.castString(s.getId()));
		text_userName.setText(s.getUserName());
		text_password.setText(s.getPassword());
		if(bool == false) {
			btn_save.setEnabled(false);
			//获取当前窗体中的所有组件,获取当前Pane中的组件个数
			int count = contentPane.getComponentCount();
			for(int i = 0;i<count;i++) {
				//通过下标获取panel中某一个组件
				 Component com = contentPane.getComponent(i);
				 //如果当前组件是JTextField就讲其改为不可编辑
				 if(com instanceof JTextField) {
					 JTextField t = (JTextField) com;
					 t.setEditable(false);
				 }else if(com instanceof JTextPane) {
					 //如果是多行文本框,也将其改为不可编辑
					 JTextPane p = (JTextPane) com;
					 p.setEditable(false);
				 }
			}
		}
	}

	/**
	 * Create the frame.
	 */
	public V_adminUpdate() {
		setTitle("修改密码");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(600, 300, 377, 404);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 10, 341, 345);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("序号：");
		lblNewLabel.setBounds(27, 56, 54, 24);
		panel.add(lblNewLabel);
		
		text_id = new JTextField();
		text_id.setEditable(false);
		text_id.setBounds(91, 58, 161, 21);
		panel.add(text_id);
		text_id.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("用户名：");
		lblNewLabel_1.setBounds(27, 104, 54, 24);
		panel.add(lblNewLabel_1);
		
		text_userName = new JTextField();
		text_userName.setBounds(91, 104, 161, 25);
		panel.add(text_userName);
		text_userName.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("登录密码：");
		lblNewLabel_2.setBounds(27, 152, 66, 24);
		panel.add(lblNewLabel_2);
		
		text_password = new JTextField();
		text_password.setBounds(91, 152, 161, 24);
		panel.add(text_password);
		text_password.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("确认密码：");
		lblNewLabel_3.setBounds(27, 198, 66, 24);
		panel.add(lblNewLabel_3);
		
		text_password2 = new JTextField();
		text_password2.setBounds(91, 199, 161, 24);
		panel.add(text_password2);
		text_password2.setColumns(10);
		
		btn_save = new JButton("确认");
		btn_save.setIcon(new ImageIcon(V_adminUpdate.class.getResource("/images/确认.png")));
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Admin s = new Admin();
				if(text_userName.getText().equals("") || text_password.getText().equals("") || text_password2.getText().equals("")) {
					JOptionPane.showMessageDialog(V_adminUpdate.this,"用户名或密码不能为空!");
				}else {
					s.setUserName(text_userName.getText());
					s.setPassword(text_password.getText());
					AdminServiceDao service = new AdminServiceDaoImpl();
					int id = CastUtil.castInt(text_id.getText());
					if(id>0 && text_password.getText().equals(text_password2.getText())) {
						s.setId(id);
						if(service.update(s)) {
							JOptionPane.showMessageDialog(V_adminUpdate.this, "修改成功!");
							dispose();
						}else {
							JOptionPane.showMessageDialog(V_adminUpdate.this, "修改失败!");
						}
					}else {
						JOptionPane.showMessageDialog(V_adminUpdate.this, "请输入一样的密码!");
					}
				}
			}
		});
		btn_save.setBounds(29, 268, 93, 23);
		panel.add(btn_save);
		
		JButton btnNewButton_1 = new JButton("退出");
		btnNewButton_1.setIcon(new ImageIcon(V_adminUpdate.class.getResource("/images/reset.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton_1.setBounds(182, 268, 93, 23);
		panel.add(btnNewButton_1);
	}
}
