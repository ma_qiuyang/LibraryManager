package org.lq.view.enroll;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import org.lq.view.admin.V_admin;
import org.lq.view.books.V_booksAdmin;
import org.lq.view.order.V_OrderList;
import org.lq.view.reader.V_Reader;
import org.lq.view.supplier.V_supplier;
import org.lq.view.sys.Auto;
import org.lq.view.sys.MysqlDataManager;

public class V_AdminManagerMain extends JFrame {

	private JPanel contentPane;
	private JTabbedPane tabbedPane;
	private static MysqlDataManager data = new MysqlDataManager();
	public static MysqlDataManager getData() {
		return data;
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_AdminManagerMain frame = new V_AdminManagerMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_AdminManagerMain() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_AdminManagerMain.class.getResource("/images/管理员.png")));
		setTitle("管理员主界面");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//		setBounds(300, 200, Double.valueOf(screenSize.getWidth()).intValue()-600, Double.valueOf(screenSize.getHeight()).intValue()-200);
		setBounds(100, 100, 1254, 692);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JButton btnNewButton = new JButton("读者管理");
		btnNewButton.setIcon(new ImageIcon(V_AdminManagerMain.class.getResource("/images/读者管理.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tabbedPane.removeAll();
				tabbedPane.addTab("读者管理", null, new V_Reader().getContentPane(), null);
				tabbedPane.updateUI();
			}
		});
		menuBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("管理员管理");
		btnNewButton_1.setIcon(new ImageIcon(V_AdminManagerMain.class.getResource("/images/管理员.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tabbedPane.removeAll();
				tabbedPane.addTab("管理员管理",new V_admin().getContentPane());
				tabbedPane.updateUI();
			}
		});
		menuBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("图书管理");
		btnNewButton_2.setIcon(new ImageIcon(V_AdminManagerMain.class.getResource("/images/图书(1).png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tabbedPane.removeAll();
				tabbedPane.addTab("图书管理", null, new V_booksAdmin().getContentPane(), null);
				tabbedPane.updateUI();
			}
		});
		menuBar.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("图书入库");
		btnNewButton_3.setIcon(new ImageIcon(V_AdminManagerMain.class.getResource("/images/_入库.png")));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tabbedPane.removeAll();
				tabbedPane.addTab("图书入库", null, new V_OrderList().getContentPane(), null);
				tabbedPane.updateUI();
			}
		});
		menuBar.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("供货商管理");
		btnNewButton_4.setIcon(new ImageIcon(V_AdminManagerMain.class.getResource("/images/供货商.png")));
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tabbedPane.removeAll();
				tabbedPane.addTab("供货商管理", null, new V_supplier().getContentPane(), null);
				tabbedPane.updateUI();
			}
		});
		
		menuBar.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("数据操作");
		btnNewButton_5.setIcon(new ImageIcon(V_AdminManagerMain.class.getResource("/images/数据.png")));
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tabbedPane.removeAll();
				tabbedPane.addTab("数据操作", null, data.getContentPane(), null);
				tabbedPane.updateUI();
			}
		});
		menuBar.add(btnNewButton_5);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		tabbedPane = new JTabbedPane(JTabbedPane.RIGHT);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JLabel lblNewLabel = new JLabel(" ");
		lblNewLabel.setBackground(Color.LIGHT_GRAY);
		lblNewLabel.setIcon(new ImageIcon(V_AdminManagerMain.class.getResource("/images/background.png")));
		tabbedPane.addTab("", null, lblNewLabel, null);
		
//		启动备份线程
		Auto a1 = new Auto();
		Thread thread = new Thread(a1);
		thread.start();
	}

}
