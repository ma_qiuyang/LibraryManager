package org.lq.view.enroll;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.lq.entity.Admin;
import org.lq.entity.Reader;
import org.lq.service.AdminServiceDao;
import org.lq.service.ReaderService;
import org.lq.service.impl.AdminServiceDaoImpl;
import org.lq.service.impl.ReaderServiceImpl;
import org.lq.view.books.V_booksUser;
import org.lq.view.reader.V_Reader;
import org.lq.view.reader.V_ReaderAdd;
import org.lq.view.reader.V_readerMain;

public class V_Enroll extends JFrame {

	private JPanel contentPane;
	private JTextField txt_userName;
	private JRadioButton rb_admin;
	private JRadioButton rb_reader;
	private AdminServiceDao as = new AdminServiceDaoImpl();
	private ReaderService rs = new ReaderServiceImpl();
	
	private static int id;
	private JPasswordField txt_password;
	public int getId() {
		return this.id;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_Enroll frame = new V_Enroll();
					frame.setBounds(700, 250, frame.getWidth(), frame.getHeight());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_Enroll() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_Enroll.class.getResource("/images/登录.png")));
		setResizable(false);
		setTitle("登录界面");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(700, 250, 473, 496);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.controlHighlight);
		panel.setBounds(10, 10, 447, 437);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("用户名 ：");
		lblNewLabel.setIcon(new ImageIcon(V_Enroll.class.getResource("/images/用户名.png")));
		lblNewLabel.setBounds(59, 88, 80, 30);
		panel.add(lblNewLabel);
		
		txt_userName = new JTextField();
		txt_userName.setBounds(158, 88, 198, 30);
		panel.add(txt_userName);
		txt_userName.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("密码 ：");
		lblNewLabel_1.setIcon(new ImageIcon(V_Enroll.class.getResource("/images/登录-密码.png")));
		lblNewLabel_1.setBounds(59, 163, 80, 30);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("身份 ：");
		lblNewLabel_2.setIcon(new ImageIcon(V_Enroll.class.getResource("/images/身份证.png")));
		lblNewLabel_2.setBounds(59, 239, 80, 30);
		panel.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("登录");
		btnNewButton.setIcon(new ImageIcon(V_Enroll.class.getResource("/images/登录.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String userName = txt_userName.getText();
				String password = txt_password.getText();
				if(rb_admin.isSelected()) {
					try {
						Admin admin = as.getByUser(userName, password);
						id = admin.getId();
						V_AdminManagerMain main = new V_AdminManagerMain();
						main.setVisible(true);
						dispose();
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(V_Enroll.this, "没有该用户,请重新登录","登录",JOptionPane.ERROR_MESSAGE);
						txt_userName.setText("");
						txt_password.setText("");
					}
				}else {
					try {
						Reader reader = rs.login(userName, password);
						id = reader.getId();
						V_readerMain main = new V_readerMain(id);
						main.setVisible(true);
						dispose();
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(V_Enroll.this, "没有该用户,请重新登录","登录",JOptionPane.ERROR_MESSAGE);
						txt_userName.setText("");
						txt_password.setText("");
					}
				}
			}
		});
		btnNewButton.setBounds(59, 339, 93, 30);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("退出");
		btnNewButton_1.setIcon(new ImageIcon(V_Enroll.class.getResource("/images/reset.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton_1.setBounds(174, 339, 93, 30);
		panel.add(btnNewButton_1);
		
		rb_admin = new JRadioButton("管理员");
		rb_admin.setSelected(true);
		rb_admin.setBounds(156, 243, 101, 23);
		panel.add(rb_admin);
		
		rb_reader = new JRadioButton("读者");
		rb_reader.setBounds(283, 243, 93, 23);
		panel.add(rb_reader);
		//只能选择一个
		ButtonGroup bg = new ButtonGroup();
		bg.add(rb_admin);
		bg.add(rb_reader);
		
		JButton btnNewButton_2 = new JButton("免登录查询图书");
		btnNewButton_2.setIcon(new ImageIcon(V_Enroll.class.getResource("/images/查看.png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				V_booksUser booksUser = new V_booksUser();
				booksUser.setBounds(V_Enroll.this.getX()-200, V_Enroll.this.getY(), booksUser.getWidth(), booksUser.getHeight());
				booksUser.setVisible(true);
				booksUser.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						V_Enroll v= new V_Enroll();
						v.setBounds(700, 250, v.getWidth(), v.getHeight());
						v.setVisible(true);
					}
				});
			}
		});
		btnNewButton_2.setBounds(287, 339, 140, 30);
		panel.add(btnNewButton_2);
		
		JLabel label = new JLabel("图书管理系统");
		label.setBackground(Color.WHITE);
		label.setIcon(new ImageIcon(V_Enroll.class.getResource("/images/图书.png")));
		label.setForeground(Color.BLUE);
		label.setFont(new Font("宋体", Font.BOLD, 33));
		label.setBounds(97, 10, 259, 53);
		panel.add(label);
		
		txt_password = new JPasswordField();
		txt_password.setBounds(158, 163, 198, 30);
		panel.add(txt_password);
		
		JButton btnNewButton_3 = new JButton("学生注册");
		btnNewButton_3.setIcon(new ImageIcon(V_Enroll.class.getResource("/images/注册.png")));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_ReaderAdd add = new V_ReaderAdd();
				add.setBounds(V_Enroll.this.getX()+100, V_Enroll.this.getY(), add.getWidth(), add.getHeight());
				add.setVisible(true);
			}
		});
		btnNewButton_3.setBounds(158, 393, 114, 23);
		panel.add(btnNewButton_3);
	}
}
