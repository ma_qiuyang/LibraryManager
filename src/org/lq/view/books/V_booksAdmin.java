package org.lq.view.books;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.lq.entity.Books;
import org.lq.entity.Dictionary;
import org.lq.entity.TypeID;
import org.lq.service.BooksService;
import org.lq.service.DictionaryService;
import org.lq.service.impl.BooksServiceImpl;
import org.lq.service.impl.DictionaryServiceImpl;
import org.lq.util.CastUtil;
import org.lq.view.dic.V_Dictionary;
/**
 * 图书操作界面（管理员使用）
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.view.books
 * @date 2020年8月23日下午2:52:35
 */
public class V_booksAdmin extends JFrame {

	private JPanel contentPane;
	private static JTable table;
	private JTextField txt_find;
	
	BooksService bs = new BooksServiceImpl();
	private JCheckBox cb_status;
	private JComboBox cb_search;
	private int booksId;//右键选中行的图书id
	private JTree tree;
	public int getBooksId() {
		return booksId;
	}
	//借阅界面传入一个值
	private int extra = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_booksAdmin frame = new V_booksAdmin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_booksAdmin() {
		setTitle("管理员界面");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 959, 492);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "\u56FE\u4E66\u64CD\u4F5C", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("查询方式：");
		panel.add(lblNewLabel);
		
		cb_search = new JComboBox();
		cb_search.setModel(new DefaultComboBoxModel(new String[] {"-请选择-", "图书编号", "图书名称", "图书作者"}));
		panel.add(cb_search);
		
		txt_find = new JTextField();
		panel.add(txt_find);
		txt_find.setColumns(10);
		
		JButton btnNewButton = new JButton("开始查询");
		btnNewButton.setIcon(new ImageIcon(V_booksAdmin.class.getResource("/images/查询.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String result = cb_search.getSelectedItem().toString();
				switch (result) {
				case "图书编号":
					//显示可借，且编号为指定编号的图书
					if (cb_status.isSelected()==true) {
						List<Books> byBookNumber = bs.getByBookNumber(txt_find.getText(),1);
						initTable(byBookNumber);
					}else {//否则模糊查询编号
						List<Books> byBookNumber = bs.getByBookNumber(txt_find.getText());
						initTable(byBookNumber);
					}
					break;
				case "图书名称":
					if (cb_status.isSelected()==true) {
						initTable(bs.getByBookName(txt_find.getText(),1));
					}else {
						initTable(bs.getByBookName(txt_find.getText()));
					}
					break;
				case "图书作者":
					if (cb_status.isSelected()==true) {
						initTable(bs.getByAuthor(txt_find.getText(),1));
					}else {
						initTable(bs.getByAuthor(txt_find.getText()));
					}
					break;
				default:
					JOptionPane.showMessageDialog(V_booksAdmin.this, "请选择查询类型，再进行查询");
					break;
				}
			}
		});
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("查询所有");
		btnNewButton_1.setIcon(new ImageIcon(V_booksAdmin.class.getResource("/images/查看.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_find.setText("");
				cb_search.setSelectedIndex(0);
				if (cb_status.isSelected()) {
					initTable(bs.getByStatus(1));
				}else {
					initTable(bs.findAll());
				}
			}
		});
		panel.add(btnNewButton_1);
		
		cb_status = new JCheckBox("只显示可借图书");
		cb_status.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cb_status.isSelected()&&txt_find.getText().equals("")) {
					//1.显示所有可借的图书
					initTable(bs.getByStatus(1));
				}else if (cb_status.isSelected()==false&&txt_find.getText().equals("")) {
					//2.显示所有图书
					initTable(bs.findAll());
				}else if (cb_search.getSelectedIndex()==1&&!txt_find.getText().equals("")&&cb_status.isSelected()) {
					//3.显示按图书编号查询出来的结果的可借图书
					initTable(bs.getByBookNumber(txt_find.getText(), 1));
				}else if (cb_search.getSelectedIndex()==1&&!txt_find.getText().equals("")&&!cb_status.isSelected()) {
					//4.显示按图书编号查询出来的结果的所有图书
					initTable(bs.getByBookNumber(txt_find.getText()));
				}else if (cb_search.getSelectedIndex()==2&&!txt_find.getText().equals("")&&cb_status.isSelected()) {
					//5.显示按图书名字查询出来的结果的可借图书
					initTable(bs.getByBookName(txt_find.getText(), 1));
				}else if (cb_search.getSelectedIndex()==2&&!txt_find.getText().equals("")&&!cb_status.isSelected()) {
					//6.显示按图书名字查询出来的结果的所有图书
					initTable(bs.getByBookName(txt_find.getText()));
				}else if (cb_search.getSelectedIndex()==3&&!txt_find.getText().equals("")&&cb_status.isSelected()) {
					//7.显示按图书作者查询出来的结果的可借图书
					initTable(bs.getByAuthor(txt_find.getText(), 1));
				}if (cb_search.getSelectedIndex()==3&&!txt_find.getText().equals("")&&!cb_status.isSelected()) {
					//8.显示按图书作者查询出来的结果的所有图书
					initTable(bs.getByAuthor(txt_find.getText()));
				}
			}
		});
		panel.add(cb_status);
		
		JButton btnNewButton_2 = new JButton("详细信息");
		btnNewButton_2.setIcon(new ImageIcon(V_booksAdmin.class.getResource("/images/详细信息.png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//先获取选中的行数
				int index = table.getSelectedRow();
				if (index >=0) {
					int id = CastUtil.castInt(table.getValueAt(index, 0));//获取隐藏的id
					V_booksShowUser show = new V_booksShowUser(id,false);
					show.setBounds(V_booksAdmin.this.getX()+300, V_booksAdmin.this.getY(), show.getWidth(), show.getHeight());
					show.setVisible(true);
				}else {
					JOptionPane.showMessageDialog(V_booksAdmin.this, "请先选择要查看详细信息的图书后，再点击此按钮");
				}
			}
		});
		panel.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("添加图书");
		btnNewButton_3.setIcon(new ImageIcon(V_booksAdmin.class.getResource("/images/add.png")));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_booksShow show = new V_booksShow();
				show.setBounds(V_booksAdmin.this.getX()+300, V_booksAdmin.this.getY(), show.getWidth(), show.getHeight());
				show.setVisible(true);
				show.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						initTable(bs.findAll());
						initTree();
					}
				});
			}
		});
		panel.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("管理图书类型");
		btnNewButton_4.setIcon(new ImageIcon(V_booksAdmin.class.getResource("/images/管理.png")));
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_Dictionary v = new V_Dictionary();
				v.setBounds(V_booksAdmin.this.getX()+600, V_booksAdmin.this.getY()+300, v.getWidth(), v.getHeight());
				v.setVisible(true);
				v.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						initTable(bs.findAll());
						initTree();
					}
				});
			}
		});
		panel.add(btnNewButton_4);
		
		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		tree = new JTree();
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode dmt = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
				if (!"所有类型".equals(dmt.toString())) {
					Dictionary d = (Dictionary) dmt.getUserObject();
					List<Books> list = bs.getByBookType(d.getId());
					initTable(list);
				}else {
					initTable(bs.findAll());
				}
			}
		});
		splitPane.setLeftComponent(tree);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null ,null,null}
			},
			new String[] {
				"id", "\u56FE\u4E66\u7F16\u53F7", "\u56FE\u4E66\u540D\u79F0", "\u62FC\u97F3\u7B80\u7801", "\u56FE\u4E66\u7C7B\u578B", "\u4F5C\u8005", "\u5E93\u5B58\u6570\u91CF", "\u56FE\u4E66\u6700\u5927\u6570\u91CF", "\u53EF\u501F\u72B6\u6001", "\u6240\u5728\u4F4D\u7F6E"
			}
		) {
			boolean[] columnEditables = new boolean[] { true, false, false, false, false, false, false, false,false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(8).setPreferredWidth(115);
		scrollPane.setViewportView(table);
		
		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(table, popupMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("修改");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//获取选中行的图书id
				int index = table.getSelectedRow();
				booksId = CastUtil.castInt(table.getValueAt(index, 0));
				V_booksShow booksShow = new V_booksShow(booksId, true);
				booksShow.setBounds(V_booksAdmin.this.getX()+200, V_booksAdmin.this.getY()+50, booksShow.getWidth(), booksShow.getHeight());
				booksShow.setVisible(true);
				booksShow.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						initTable(bs.findAll());
					}
				});
			}
		});
		popupMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("删除");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				booksId = CastUtil.castInt(table.getValueAt(index, 0));
				Books book = bs.getById(booksId);
				if (book.getBookCount() == book.getMaxCount()) {
					if (index >= 0) {
						int n = JOptionPane.showConfirmDialog(V_booksAdmin.this,"确定删除吗？", "删除",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
						if (n==0) {
							if (bs.delete(booksId)) {
								JOptionPane.showMessageDialog(V_booksAdmin.this, "删除成功");
								initTable(bs.findAll());
							}else {
								JOptionPane.showMessageDialog(V_booksAdmin.this, "删除失败");
							}
						}
					}
				}else {
					JOptionPane.showMessageDialog(V_booksAdmin.this, "当前该图书尚未归还完毕，无法删除！");
				}
			}
		});
		popupMenu.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("选择");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				if (index >=0 ) {
					//右键选择获取所在行的图书id
					booksId = CastUtil.castInt(table.getValueAt(index, 0));
					//借阅界面传入1，关闭窗口
					if (extra==1) {
						dispose();
					}
				}
			}
		});
		popupMenu.add(mntmNewMenuItem_2);
		
		initTree();
		initTable(bs.findAll());
	}
	/**
	 * 带参构造，显示符合传入值的集合（值可以是编号、书名、拼音简码，都可以进行模糊查询）
	 * 传入一个额外的数字，如果是1的话，选择完毕后将关闭窗口，否则不做任何操作
	 * @param value
	 */
	public V_booksAdmin(String value,int extra) {
		this();
		this.extra = extra;
		initTable(bs.query(value));
	}
	public V_booksAdmin(int extra) {
		this();
		this.extra = extra;
	}
	
	/**
	 * 初始化表格显示
	 * @param list
	 */
	public void initTable(List<Books> list) {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		//先把表格中数据清除
		int count = dtm.getRowCount();//获取行数
		for (int i = 0; i < count; i++) {
			dtm.removeRow(0);
		}
		//将集合中的书展示出来
		for (Books b : list) {
			String status = b.getStatus()==0?"不可借":"可借";
			DictionaryService ds = new DictionaryServiceImpl();
			Dictionary dic = ds.getById(b.getBookType());
			dtm.addRow(new Object[] {
					b.getId(),//隐藏列，用来获取id
					b.getBookNumber(),//编号
					b.getBookName(),//名称
					b.getPinYinCode(),//简码
					dic.getName(),
					b.getAuthor(),//作者
					b.getBookCount(),//库存数量
					b.getMaxCount(),//最大数量
					status,
					b.getLocation()//所在位置
			});
		}
	}
	/**
	 * 初始化树
	 */
	public void initTree() {
		DictionaryService ds = new DictionaryServiceImpl();
		List<Dictionary> list = ds.findByTypeid(TypeID.图书类别);//由于字典业务中传参设为int,此处0即代表图书类别，可在枚举类中查到
		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("所有类型");
		for (Dictionary type : list) {
			dmtn.add(new DefaultMutableTreeNode(type));
		}
		DefaultTreeModel dtm = new DefaultTreeModel(dmtn);
		tree.setModel(dtm);
	}
	
	
	/**
	 * 右键菜单操作
	 * @param component
	 * @param popup
	 */
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				int index = table.rowAtPoint(e.getPoint());// 返回右键所选中的行数
				table.setRowSelectionInterval(index, index);//设置选中
				popup.show(e.getComponent(), e.getX(), e.getY());//右键菜单显示位置
			}
		});
	}
}
