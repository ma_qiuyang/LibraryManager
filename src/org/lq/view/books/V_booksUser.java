package org.lq.view.books;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.lq.entity.Books;
import org.lq.entity.Dictionary;
import org.lq.entity.TypeID;
import org.lq.service.BooksService;
import org.lq.service.DictionaryService;
import org.lq.service.impl.BooksServiceImpl;
import org.lq.service.impl.DictionaryServiceImpl;
import org.lq.util.CastUtil;

import com.sun.source.tree.Tree;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
/**
 * 图书操作界面（用户使用）
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.view.books
 * @date 2020年8月23日下午2:52:16
 */
public class V_booksUser extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField txt_find;
	
	BooksService bs = new BooksServiceImpl();
	private JCheckBox cb_status;
	private JComboBox cb_search;
	private JTree tree;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_booksUser frame = new V_booksUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_booksUser() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("用户界面");
		setBounds(100, 100, 959, 492);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "\u56FE\u4E66\u64CD\u4F5C", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("查询方式：");
		panel.add(lblNewLabel);
		
		cb_search = new JComboBox();
		cb_search.setModel(new DefaultComboBoxModel(new String[] {"-请选择-", "图书编号", "图书名称", "图书作者"}));
		panel.add(cb_search);
		
		txt_find = new JTextField();
		panel.add(txt_find);
		txt_find.setColumns(10);
		
		JButton btnNewButton = new JButton("开始查询");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String result = cb_search.getSelectedItem().toString();
				switch (result) {
				case "图书编号":
					//显示可借，且编号为指定编号的图书
					if (cb_status.isSelected()==true) {
						List<Books> byBookNumber = bs.getByBookNumber(txt_find.getText(),1);
						initTable(byBookNumber);
					}else {//否则模糊查询编号
						List<Books> byBookNumber = bs.getByBookNumber(txt_find.getText());
						initTable(byBookNumber);
					}
					break;
				case "图书名称":
					if (cb_status.isSelected()==true) {
						initTable(bs.getByBookName(txt_find.getText(),1));
					}else {
						initTable(bs.getByBookName(txt_find.getText()));
					}
					break;
				case "图书作者":
					if (cb_status.isSelected()==true) {
						initTable(bs.getByAuthor(txt_find.getText(),1));
					}else {
						initTable(bs.getByAuthor(txt_find.getText()));
					}
					break;
				default:
					JOptionPane.showMessageDialog(V_booksUser.this, "请选择查询类型，再进行查询");
					break;
				}
			}
		});
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("查询所有");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_find.setText("");
				cb_search.setSelectedIndex(0);
				if (cb_status.isSelected()) {
					initTable(bs.getByStatus(1));
				}else {
					initTable(bs.findAll());
				}
			}
		});
		panel.add(btnNewButton_1);
		
		cb_status = new JCheckBox("只显示可借图书");
		cb_status.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cb_status.isSelected()&&txt_find.getText().equals("")) {
					//1.显示所有可借的图书
					initTable(bs.getByStatus(1));
				}else if (cb_status.isSelected()==false&&txt_find.getText().equals("")) {
					//2.显示所有图书
					initTable(bs.findAll());
				}else if (cb_search.getSelectedIndex()==1&&!txt_find.getText().equals("")&&cb_status.isSelected()) {
					//3.显示按图书编号查询出来的结果的可借图书
					initTable(bs.getByBookNumber(txt_find.getText(), 1));
				}else if (cb_search.getSelectedIndex()==1&&!txt_find.getText().equals("")&&!cb_status.isSelected()) {
					//4.显示按图书编号查询出来的结果的所有图书
					initTable(bs.getByBookNumber(txt_find.getText()));
				}else if (cb_search.getSelectedIndex()==2&&!txt_find.getText().equals("")&&cb_status.isSelected()) {
					//5.显示按图书名字查询出来的结果的可借图书
					initTable(bs.getByBookName(txt_find.getText(), 1));
				}else if (cb_search.getSelectedIndex()==2&&!txt_find.getText().equals("")&&!cb_status.isSelected()) {
					//6.显示按图书名字查询出来的结果的所有图书
					initTable(bs.getByBookName(txt_find.getText()));
				}else if (cb_search.getSelectedIndex()==3&&!txt_find.getText().equals("")&&cb_status.isSelected()) {
					//7.显示按图书作者查询出来的结果的可借图书
					initTable(bs.getByAuthor(txt_find.getText(), 1));
				}if (cb_search.getSelectedIndex()==3&&!txt_find.getText().equals("")&&!cb_status.isSelected()) {
					//8.显示按图书作者查询出来的结果的所有图书
					initTable(bs.getByAuthor(txt_find.getText()));
				}
			}
		});
		panel.add(cb_status);
		
		JButton btnNewButton_2 = new JButton("详细信息");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//先获取选中的行数
				int index = table.getSelectedRow();
				if (index >=0) {
					int id = CastUtil.castInt(table.getValueAt(index, 0));//获取隐藏的id
					V_booksShowUser show = new V_booksShowUser(id,false);
					show.setBounds(V_booksUser.this.getX()+300, V_booksUser.this.getY(), show.getWidth(), show.getHeight());
					show.setVisible(true);
				}else {
					JOptionPane.showMessageDialog(V_booksUser.this, "请先选择要查看详细信息的图书后，再点击此按钮");
				}
			}
		});
		panel.add(btnNewButton_2);
		
		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		tree = new JTree();
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode dmt = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
				if (!"所有类型".equals(dmt.toString())) {
					Dictionary d = (Dictionary) dmt.getUserObject();
					List<Books> list = bs.getByBookType(d.getId());
					initTable(list);
				}else {
					initTable(bs.findAll());
				}
			}
		});
		splitPane.setLeftComponent(tree);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null ,null,null}
			},
			new String[] {
				"id", "\u56FE\u4E66\u7F16\u53F7", "\u56FE\u4E66\u540D\u79F0", "\u62FC\u97F3\u7B80\u7801", "\u56FE\u4E66\u7C7B\u578B", "\u4F5C\u8005", "\u5E93\u5B58\u6570\u91CF", "\u56FE\u4E66\u6700\u5927\u6570\u91CF", "\u53EF\u501F\u72B6\u6001", "\u6240\u5728\u4F4D\u7F6E"
			}
		) {
			boolean[] columnEditables = new boolean[] { true, false, false, false, false, false, false, false,false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(8).setPreferredWidth(115);
		scrollPane.setViewportView(table);
		
		initTree();
		initTable(bs.findAll());
	}
	/**
	 * 初始化表格显示
	 * @param list
	 */
	public void initTable(List<Books> list) {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		//先把表格中数据清除
		int count = dtm.getRowCount();//获取行数
		for (int i = 0; i < count; i++) {
			dtm.removeRow(0);
		}
		//将集合中的书展示出来
		for (Books b : list) {
			String status = b.getStatus()==0?"不可借":"可借";
			DictionaryService ds = new DictionaryServiceImpl();
			Dictionary dic = ds.getById(b.getBookType());
			dtm.addRow(new Object[] {
					b.getId(),//隐藏列，用来获取id
					b.getBookNumber(),//编号
					b.getBookName(),//名称
					b.getPinYinCode(),//简码
					dic.getName(),
					b.getAuthor(),//作者
					b.getBookCount(),//库存数量
					b.getMaxCount(),//最大数量
					status,
					b.getLocation()//所在位置
			});
		}
	}
	/**
	 * 初始化树
	 */
	public void initTree() {
		DictionaryService ds = new DictionaryServiceImpl();
		List<Dictionary> list = ds.findByTypeid(TypeID.图书类别);//由于字典业务中传参设为int,此处0即代表图书类别，可在枚举类中查到
		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("所有类型");
		for (Dictionary type : list) {
			dmtn.add(new DefaultMutableTreeNode(type));
		}
		DefaultTreeModel dtm = new DefaultTreeModel(dmtn);
		tree.setModel(dtm);
	}
	
	
}
