package org.lq.view.books;

import javax.swing.text.AttributeSet;
import javax.swing.text.PlainDocument;

/**
 * 重载insertString方法，实现限制文本框只能输入数字
 * 
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.view
 * @date 2020年8月23日上午11:13:40
 */
public class NumberTextField extends PlainDocument {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NumberTextField() {
		super();
	}

	public void insertString(int offset, String str, AttributeSet attr) throws javax.swing.text.BadLocationException {
		if (str == null) {
			return;
		}

		char[] s = str.toCharArray();
		int length = 0;
		// 过滤非数字
		for (int i = 0; i < s.length; i++) {
			if ((s[i] >= '0') && (s[i] <= '9')) {
				s[length++] = s[i];
			}
			// 插入内容
		}
		super.insertString(offset, new String(s, 0, length), attr);
	}
}