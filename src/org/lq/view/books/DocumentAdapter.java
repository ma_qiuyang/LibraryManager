package org.lq.view.books;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
/**
 * 适配器设计模式
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.view
 * @date 2020年8月21日下午10:06:02
 */
public abstract class DocumentAdapter implements DocumentListener {
	
	@Override
	public void changedUpdate(DocumentEvent e) {
	}
	@Override
	public void insertUpdate(DocumentEvent e) {
	}
	@Override
	public void removeUpdate(DocumentEvent e) {
	}
	
}
