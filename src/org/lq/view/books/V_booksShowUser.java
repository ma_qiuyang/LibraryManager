package org.lq.view.books;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.Segment;

import org.lq.entity.Books;
import org.lq.entity.Dictionary;
import org.lq.entity.TypeID;
import org.lq.service.BooksService;
import org.lq.service.DictionaryService;
import org.lq.service.impl.BooksServiceImpl;
import org.lq.service.impl.DictionaryServiceImpl;
import org.lq.util.CastUtil;
import org.lq.util.FileUpload;
import org.lq.util.PinYinUtil;
import org.lq.view.dic.V_Dictionary;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
/**
 * 显示图书信息（仅查询使用）
 * @author 胡晓佟
 * @phone 15136721623
 * @package org.lq.view.books
 * @date 2020年8月23日下午2:52:59
 */
public class V_booksShowUser extends JFrame {

	private JPanel contentPane;
	private JTextField txt_number;
	private JTextField txt_name;
	private JTextField txt_pinYin;
	private JTextField txt_author;
	private JTextField txt_count;
	private JTextField txt_maxCount;
	private JTextField txt_status;
	private JTextField txt_location;
	private File selectFile;
	private JLabel lbl_icon;
	private String iconPath = "";// 存储图片文件路径
	private boolean flag = true;// 获取是添加还是回显数据
	public int id = -1;// 进入此窗体传入的图书id值

	BooksService bs = new BooksServiceImpl();
	private JComboBox cb_type;
	private JEditorPane txt_desc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_booksShowUser frame = new V_booksShowUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_booksShowUser() {
		setTitle("图书操作");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 483, 581);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("图书编号：");
		lblNewLabel.setBounds(39, 26, 90, 24);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("图书名称：");
		lblNewLabel_1.setBounds(39, 71, 90, 24);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_1_1 = new JLabel("拼音简码：");
		lblNewLabel_1_1.setBounds(39, 119, 90, 24);
		contentPane.add(lblNewLabel_1_1);

		JLabel lblNewLabel_1_1_1 = new JLabel("图书作者：");
		lblNewLabel_1_1_1.setBounds(39, 170, 90, 24);
		contentPane.add(lblNewLabel_1_1_1);

		JLabel lblNewLabel_1_1_1_1 = new JLabel("图书描述：");
		lblNewLabel_1_1_1_1.setBounds(236, 213, 90, 24);
		contentPane.add(lblNewLabel_1_1_1_1);

		JLabel lblNewLabel_1_1_1_1_1 = new JLabel("库存数量：");
		lblNewLabel_1_1_1_1_1.setBounds(39, 318, 90, 24);
		contentPane.add(lblNewLabel_1_1_1_1_1);

		JLabel lblNewLabel_1_1_1_1_1_1 = new JLabel("最大数量：");
		lblNewLabel_1_1_1_1_1_1.setBounds(39, 365, 90, 24);
		contentPane.add(lblNewLabel_1_1_1_1_1_1);

		JLabel lblNewLabel_1_1_1_1_1_1_1 = new JLabel("可借状态：");
		lblNewLabel_1_1_1_1_1_1_1.setBounds(39, 414, 90, 24);
		contentPane.add(lblNewLabel_1_1_1_1_1_1_1);

		// 为确定按钮添加事件，如果flag为false，代表使用的是查看信息，点击确定为关闭窗口
		// 如果flag为true，代表使用的是修改信息，点击确定为修改操作
		JButton btn_save = new JButton("确定");
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (flag == false) {// 只进行查看图书信息，会进入此分支
					dispose();
				} else if (flag == true) {// flag默认为true,只有从查看信息按钮进入本窗体才会将其修改为false
					// 添加、修改进入下面分支
					Books b = new Books();
					b.setBookNumber(txt_number.getText());
					b.setBookName(txt_name.getText());
					b.setPinYinCode(txt_pinYin.getText());
					b.setBookCount(CastUtil.castInt(txt_count.getText()));
					b.setStatus(CastUtil.castInt(txt_count.getText()) > 1 ? 1 : 0);// 判断输入的库存数量是否大于1
					b.setAuthor(txt_author.getText());
					b.setMaxCount(CastUtil.castInt(txt_maxCount.getText()));
					b.setDescription(txt_desc.getText());
					b.setLocation(txt_location.getText());

					//给图书类型赋值
					ComboBoxModel model = cb_type.getModel();
					Dictionary dic = (Dictionary) model.getSelectedItem();
					b.setBookType(dic.getId());
					
					// 图片复制
					String dir = System.getProperty("user.dir");
//					if (selectFile == null) {
//						JOptionPane.showMessageDialog(V_booksShow.this, "请选择图书封面！！！");
//					} 
					if (selectFile!=null) {
						String fileName = "/books-imgs/" + System.currentTimeMillis() + selectFile.getName();
						FileUpload.copy(selectFile.getAbsolutePath(), dir + fileName);
						// 图书封面赋值
						b.setBookIcon(fileName);
					}

					if (id == -1) {
						if (bs.save(b)) {
							JOptionPane.showMessageDialog(V_booksShowUser.this, "添加成功");
							dispose();
						} else {
							JOptionPane.showMessageDialog(V_booksShowUser.this, "添加失败");
						}
					} else {
						b.setId(id);
						if (bs.update(b)) {
							JOptionPane.showMessageDialog(V_booksShowUser.this, "修改成功");
							dispose();
						} else {
							JOptionPane.showMessageDialog(V_booksShowUser.this, "修改失败");
						}
					}

				}

			}
		});
		btn_save.setBounds(90, 490, 97, 23);
		contentPane.add(btn_save);

		JButton btnNewButton_1 = new JButton("取消");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton_1.setBounds(286, 490, 97, 23);
		contentPane.add(btnNewButton_1);

		txt_number = new JTextField();
		txt_number.setBounds(110, 28, 90, 21);
		contentPane.add(txt_number);
		txt_number.setColumns(10);
		
		txt_number.setDocument(new NumberTextField());

		txt_name = new JTextField();
		txt_name.setColumns(10);
		// 获取当前文本框内容的对象
		Document doc = txt_name.getDocument();
		doc.addDocumentListener(new DocumentAdapter() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				String py = PinYinUtil.getFirstPY(txt_name.getText());
				txt_pinYin.setText(py);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				String py = PinYinUtil.getFirstPY(txt_name.getText());
				txt_pinYin.setText(py);
			}
		});
		txt_name.setBounds(110, 73, 90, 21);
		contentPane.add(txt_name);

		txt_pinYin = new JTextField();
		txt_pinYin.setEditable(false);
		txt_pinYin.setColumns(10);
		txt_pinYin.setBounds(110, 121, 90, 21);
		contentPane.add(txt_pinYin);

		txt_author = new JTextField();
		txt_author.setColumns(10);
		txt_author.setBounds(110, 172, 90, 21);
		contentPane.add(txt_author);

		JLabel lblNewLabel_2 = new JLabel("图书封面：");
		lblNewLabel_2.setBounds(236, 31, 71, 15);
		contentPane.add(lblNewLabel_2);

		lbl_icon = new JLabel("暂无封面");
//		此界面仅作展示，取消封面的双击事件
//		lbl_icon.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseClicked(MouseEvent e) {
//				if (e.getClickCount() == 2) {// 双击
//					JFileChooser choose = new JFileChooser();
//					choose.showOpenDialog(V_booksShowUser.this);
//					choose.setFileFilter(new FileNameExtensionFilter("JPG & PNG fiels", "jpg", "png"));// 匹配指定格式文件
//					selectFile = choose.getSelectedFile();
//					if (selectFile != null && selectFile.exists()) {
//						// 将选择的图片显示出来
//						ImageIcon icon = new ImageIcon(selectFile.getAbsolutePath());
//						icon.setImage(icon.getImage().getScaledInstance(133, 144, 1));
//						lbl_icon.setIcon(icon);
//					}
//
//				}
//			}
//		});
		lbl_icon.setIcon(new ImageIcon(V_booksShowUser.class.getResource("/images/tu.png")));
		lbl_icon.setBounds(300, 49, 134, 145);
		contentPane.add(lbl_icon);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(264, 242, 170, 165);
		contentPane.add(scrollPane);

		txt_desc = new JEditorPane();
		scrollPane.setViewportView(txt_desc);

		JLabel lblNewLabel_1_1_1_2 = new JLabel("图书类型：");
		lblNewLabel_1_1_1_2.setBounds(39, 223, 90, 24);
		contentPane.add(lblNewLabel_1_1_1_2);

		cb_type = new JComboBox();
		cb_type.setBounds(110, 224, 90, 23);
		contentPane.add(cb_type);

		txt_count = new JTextField();
		txt_count.setColumns(10);
		// 设置文本框只能输入数字
		txt_count.setDocument(new NumberTextField());
		// 可借状态根据库存数量动态判断
		Document doc2 = txt_count.getDocument();
		doc2.addDocumentListener(new DocumentAdapter() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				int count = CastUtil.castInt(txt_count.getText());
				if (count > 1) {
					txt_status.setText("可借");
				} else {
					txt_status.setText("不可借");
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				int count = CastUtil.castInt(txt_count.getText());
				if (count > 1) {
					txt_status.setText("可借");
				} else {
					txt_status.setText("不可借");
				}
				if (txt_count.getText().equals("")) {
					txt_status.setText("");
				}
			}
		});

		txt_count.setBounds(110, 318, 90, 21);
		contentPane.add(txt_count);

		txt_maxCount = new JTextField();
		txt_maxCount.setDocument(new NumberTextField());
		txt_maxCount.setColumns(10);
		txt_maxCount.setBounds(110, 367, 90, 21);
		contentPane.add(txt_maxCount);

		txt_status = new JTextField();
		txt_status.setEditable(false);
		txt_status.setColumns(10);
		txt_status.setBounds(110, 416, 90, 21);
		contentPane.add(txt_status);

		JLabel lblNewLabel_1_1_1_1_1_2 = new JLabel("存放位置：");
		lblNewLabel_1_1_1_1_1_2.setBounds(39, 273, 90, 24);
		contentPane.add(lblNewLabel_1_1_1_1_1_2);

		txt_location = new JTextField();
		txt_location.setColumns(10);
		txt_location.setBounds(110, 273, 90, 21);
		contentPane.add(txt_location);
		Font f = new Font("黑体",Font.PLAIN,13);
		
		//初始化下拉列表
		initCombox();
	}

	/**
	 * 回显选中行的图书信息 (如果传入false代表是查看信息，需要把组件设为不可编辑，如果传入true，代表是修改信息)
	 * 
	 * @param id
	 * @param b
	 */
	public V_booksShowUser(int id, boolean flag) {
		this();
		this.id = id;
		this.flag = flag;
		Books b = bs.getById(id);
		txt_number.setText(b.getBookNumber());
		txt_name.setText(b.getBookName());
		txt_pinYin.setText(b.getPinYinCode());
		txt_author.setText(b.getAuthor());
		txt_location.setText(b.getLocation());
		txt_count.setText(b.getBookCount() + "");
		txt_maxCount.setText(b.getMaxCount() + "");
		txt_status.setText(b.getStatus() == 0 ? "不可借" : "可借");
		txt_desc.setText(b.getDescription());
		
		//回显图书封面
		String dir = System.getProperty("user.dir");
		ImageIcon icon =new ImageIcon(dir+"/"+b.getBookIcon());
		icon.setImage(icon.getImage().getScaledInstance(133, 120, 1));
		lbl_icon.setIcon(icon);
		
		//回显图书类型下拉框
		DictionaryService ds = new DictionaryServiceImpl();
		Dictionary dic = ds.getById(b.getBookType());
		String name = dic.getName();
		//先获取到图书类型的汉字，再取出每一个下拉框中的值进行比对，成功时回显下拉框
		ComboBoxModel model = cb_type.getModel();
		int size = model.getSize();
		for (int i = 0; i < size; i++) {
			if (name.equals(model.getElementAt(i).toString())) {
				cb_type.setSelectedIndex(i);
			}
		}
		
		if (flag == false) {
			// 显示详细信息进入此分支
			int count = contentPane.getComponentCount();// 获得窗体组件个数
			cb_type.setEditable(false);
			txt_desc.setEditable(false);// 描述信息框设为不可编辑
			for (int i = 0; i < count; i++) {
				Component com = contentPane.getComponent(i);
				if (com instanceof JTextField) {// 如果组件属于JTextField或者JTextPane，改为不可编辑
					JTextField t = (JTextField) com;
					t.setEditable(false);
				} else if (com instanceof JTextPane) {
					JTextPane p = (JTextPane) com;// 多行文本框
					p.setEditable(false);
				}
			}
		} else {
			// 进行修改进入此分支，把图书编号设为不可编辑
			txt_number.setEditable(false);
		}
	}

	/**
	 * 上传图片到指定路径下
	 */
	public String updateImg() {
		String icon = "";
		// 动态获取当前项目路径
		String dir = System.getProperty("user.dir");
		File file = new File(iconPath);// 创建图像文件
		String fileName = file.getName();// 获取文件名字
		InputStream is = null;
		OutputStream os = null;
		try {
			// 拼接输出路径
			icon = "/icons/" + System.currentTimeMillis() + "-" + fileName;
			is = new FileInputStream(file);// 源图片
			os = new FileOutputStream(dir + icon);
			byte[] b = new byte[1024];
			int len = 0;
			while ((len = is.read(b)) != -1) {
				os.write(b, 0, len);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				os.flush();
				os.close();
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return icon;
	}

	/**
	 * 初始化下拉列表
	 */
	public void initCombox() {
		DictionaryService ds = new DictionaryServiceImpl();
		List<Dictionary> list = ds.findByTypeid(TypeID.图书类别);
		
		Dictionary dic = new Dictionary();
		dic.setId(-1);
		dic.setName("-请选择-");
		dic.setTypeid(-1);
		list.add(0, dic);
		
		Dictionary[] type_array = list.toArray(new Dictionary[] {});
		DefaultComboBoxModel<Dictionary> typeDBM = new DefaultComboBoxModel<Dictionary>(type_array);
		cb_type.setModel(typeDBM);
		
	}
}
