package org.lq.view.supplier;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import org.lq.entity.Supplier;
import org.lq.service.SupplierService;
import org.lq.service.impl.SupplierServiceImpl;
import org.lq.util.CastUtil;
import javax.swing.ImageIcon;

public class V_SupplierAdd extends JFrame {

	private JPanel contentPane;
	private JTextField txt_id;
	private JTextField txt_simpleName;
	private JTextField txt_name;
	private JTextField txt_address;
	private JTextField txt_contact;
	private JTextField txt_tel;
	private JTextField txt_phone;
	private JTextField txt_card;
	private JLabel label_8;
	private JTextPane txt_desc;
	private JButton btn_save;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_SupplierAdd frame = new V_SupplierAdd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * @param id
	 */
	public V_SupplierAdd(int id,boolean bool) {
		this();
		Supplier s = new SupplierServiceImpl().getSupplierById(id);
		txt_id.setText(CastUtil.castString(s.getId()));
		txt_address.setText(s.getAddress());
		txt_card.setText(s.getACnumber());
		txt_contact.setText(s.getContacts());
		txt_name.setText(s.getName());
		txt_phone.setText(s.getCnumber());
		txt_simpleName.setText(s.getAbbre());
		txt_tel.setText(s.getTnumber());
		txt_desc.setText(s.getRemarks());
		if(bool == false) {
			btn_save.setEnabled(false);
			//获取当前窗体中的所有组件,获取当前Pane中的组件个数
			int count = contentPane.getComponentCount();
			for(int i = 0;i<count;i++) {
				//通过下标获取panel中某一个组件
				 Component com = contentPane.getComponent(i);
				 //如果当前组件是JTextField就讲其改为不可编辑
				 if(com instanceof JTextField) {
					 JTextField t = (JTextField) com;
					 t.setEditable(false);
				 }else if(com instanceof JTextPane) {
					 //如果是多行文本框,也将其改为不可编辑
					 JTextPane p = (JTextPane) com;
					 p.setEditable(false);
				 }
			}
		}
	}
	/**
	 * Create the frame.
	 */
	public V_SupplierAdd() {
		setTitle("供应商添加窗体");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 369, 455);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label = new JLabel("编 号 :");
		label.setBounds(10, 28, 54, 15);
		contentPane.add(label);

		txt_id = new JTextField();
		txt_id.setEditable(false);
		txt_id.setBounds(97, 25, 157, 21);
		contentPane.add(txt_id);
		txt_id.setColumns(10);

		JLabel label_1 = new JLabel("简 称 :");
		label_1.setBounds(10, 56, 54, 15);
		contentPane.add(label_1);

		txt_simpleName = new JTextField();
		txt_simpleName.setColumns(10);
		txt_simpleName.setBounds(97, 53, 157, 21);
		contentPane.add(txt_simpleName);

		JLabel label_2 = new JLabel("名 称 :");
		label_2.setBounds(10, 84, 54, 15);
		contentPane.add(label_2);

		txt_name = new JTextField();
		txt_name.setColumns(10);
		txt_name.setBounds(97, 81, 157, 21);
		contentPane.add(txt_name);

		JLabel label_3 = new JLabel("地 址 :");
		label_3.setBounds(10, 112, 54, 15);
		contentPane.add(label_3);

		txt_address = new JTextField();
		txt_address.setColumns(10);
		txt_address.setBounds(97, 109, 157, 21);
		contentPane.add(txt_address);

		JLabel label_4 = new JLabel("联 系 人 :");
		label_4.setBounds(10, 140, 77, 15);
		contentPane.add(label_4);

		txt_contact = new JTextField();
		txt_contact.setColumns(10);
		txt_contact.setBounds(97, 137, 157, 21);
		contentPane.add(txt_contact);

		JLabel label_5 = new JLabel("电话号码 :");
		label_5.setBounds(10, 168, 77, 15);
		contentPane.add(label_5);

		txt_tel = new JTextField();
		txt_tel.setColumns(10);
		txt_tel.setBounds(97, 165, 157, 21);
		contentPane.add(txt_tel);

		JLabel label_6 = new JLabel("手机号 :");
		label_6.setBounds(10, 196, 54, 15);
		contentPane.add(label_6);

		txt_phone = new JTextField();
		txt_phone.setColumns(10);
		txt_phone.setBounds(97, 193, 157, 21);
		contentPane.add(txt_phone);

		JLabel label_7 = new JLabel("银行卡 :");
		label_7.setBounds(10, 227, 54, 15);
		contentPane.add(label_7);

		txt_card = new JTextField();
		txt_card.setColumns(10);
		txt_card.setBounds(97, 224, 157, 21);
		contentPane.add(txt_card);

		label_8 = new JLabel("备 注 :");
		label_8.setBounds(10, 262, 54, 15);
		contentPane.add(label_8);

		txt_desc = new JTextPane();
		txt_desc.setBounds(97, 255, 157, 82);
		contentPane.add(txt_desc);

		btn_save = new JButton("保存");
		btn_save.setIcon(new ImageIcon(V_SupplierAdd.class.getResource("/images/save.png")));
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Supplier s = new Supplier();
				if(txt_name.getText().equals("") || txt_contact.getText().equals("") || txt_phone.getText().equals("") || txt_card.getText().equals("")) {
					JOptionPane.showMessageDialog(V_SupplierAdd.this,"名称，联系人，联系人电话或银行卡不能为空!");
				}else {
				s.setAbbre(txt_simpleName.getText());
				s.setACnumber(txt_card.getText());
				s.setAddress(txt_address.getText());
				s.setCnumber(txt_phone.getText());
				s.setContacts(txt_contact.getText());
				s.setName(txt_name.getText());
				s.setRemarks(txt_desc.getText());
				s.setTnumber(txt_tel.getText());

				SupplierService supplierService = new SupplierServiceImpl();
				
				int id = CastUtil.castInt(txt_id.getText());
				if(id>0) {
					s.setId(id);
					if(supplierService.updateSupplier(s)) {
						JOptionPane.showMessageDialog(V_SupplierAdd.this, "修改成功!");
						dispose();
					}else {
						JOptionPane.showMessageDialog(V_SupplierAdd.this, "修改失败!");
					}
				}else {
					if(supplierService.saveSupplier(s)) {
						JOptionPane.showMessageDialog(V_SupplierAdd.this, "添加成功!");
						dispose();
					}else {
						JOptionPane.showMessageDialog(V_SupplierAdd.this, "添加失败!");
					}
				}
				}

			}
		});
		btn_save.setBounds(40, 371, 93, 23);
		contentPane.add(btn_save);

		JButton button_1 = new JButton("取消");
		button_1.setIcon(new ImageIcon(V_SupplierAdd.class.getResource("/images/reset.png")));
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		button_1.setBounds(204, 371, 93, 23);
		contentPane.add(button_1);
	}
	

	
}

